
mkdir $SECNET_PATH/sys/certs
mkdir $SECNET_PATH/sys/certs/dovecot

mkdir $SECNET_PATH/work/dovecot
mkdir $SECNET_PATH/work/dovecot/runtime_data

mkdir $SECNET_PATH/work/dovecot/logs

#mkdir $SECNET_CERTS/dovecot

cp $SECNET_CERTS/dovecot/dovecot_cert.pem  $SECNET_PATH/sys/certs/dovecot
cp $SECNET_CERTS/dovecot/dovecot_priv.pem  $SECNET_PATH/sys/certs/dovecot

mkdir $SECNET_PATH/work/dovecot/login_dir

mkdir $SECNET_PATH/work/dovecot/sockets

mkdir $SECNET_PATH/work/mail/index

chown secnetu $SECNET_PATH/work/mail/index
chgrp secnetu $SECNET_PATH/work/mail/index
chmod 0777 $SECNET_PATH/work/mail/index

#trash conf file does not exist for now
#mkdir /secnet/sys/conf/dovecot
#cp /secnet-dev/conf/dovecot/dovecot-trash.conf /secnet/sys/conf/dovecot

#export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib"


#dovecot installation
cd $SECNET_PATH/src/dovecot-1.0.1

make clean

# --with-mysql(avoid initially, unless it provides significant advantages)   
# SSL_CFLAGS  C compiler flags for SSL, overriding pkg-config
# SSL_LIBS    linker flags for SSL, overriding pkg-config
# --with-ssl=openssl #not needed - it is the default

#:/var/lib:/lib:/usr/lib:/usr/local/lib
#CPPFLAGS="-I$SECNET_PATH/sys/openssl/include" LDFLAGS="-L$SECNET_PATH/sys/openssl/lib -lssl -lcrypto" ./configure --prefix=$SECNET_PATH/sys/dovecot --with-ssldir=$SECNET_PATH/sys/certs/dovecot
LDFLAGS="-lssl -lcrypto" ./configure --prefix=$SECNET_PATH/sys/dovecot --with-ssldir=$SECNET_PATH/sys/certs/dovecot

make

make install

cd $SECNET_INSTALL_DIR

cp $SECNET_CONF/dovecot/dovecot.conf $SECNET_PATH/sys/dovecot/etc

# server will be started by start script
#$SECNET_PATH/sys/dovecot/sbin/dovecot
