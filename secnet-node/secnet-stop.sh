export SECNET_PATH="/secnet"
export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:$LD_LIBRARY_PATH"

$SECNET_PATH/sys/apache/bin/apachectl stop                    #clean apache shutdown 
kill `cat $SECNET_PATH/work/dovecot/runtime_data/master.pid`  #clean dovecot shutdown
$SECNET_PATH/sys/postfix/command/postfix stop                 #clean postfix shutdown

