## Step 1: install openSSL
cd $SECNET_PATH/src/openssl-0.9.8g
./config shared --prefix=$SECNET_PATH/sys/openssl
make
#make test
make install

cd $SECNET_PATH/sys/openssl/lib
ln -s libssl.so.0.9.8 libssl.so.6
ln -s libcrypto.so.0.9.8 libcrypto.so.6

#echo "openSSL installed - Press <ENTER> to continue..."
#read
