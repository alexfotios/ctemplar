#export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib"

# secnet-install has a scripts dir that contains what is now in secnet-dev dir.
# Scripts dir has install and control subdirs

#mkdir $SECNET_PATH/sys/postfix
#mkdir $SECNET_PATH/sys/postfix/command
#chmod 0777 $SECNET_PATH/sys/postfix
#chmod 0777 $SECNET_PATH/sys/postfix/command
#chmod 0777 $SECNET_PATH/sys

## Step 3: postfix installation

/usr/sbin/groupadd postdrop
groupadd postdrop

#make postfix conf dir
mkdir $SECNET_PATH/sys/conf/
mkdir $SECNET_PATH/sys/conf/postfix
chown -R secnetu $SECNET_PATH/sys/conf
chgrp -R root $SECNET_PATH/sys/conf
chmod -R 1770 $SECNET_PATH/sys/conf
#chmod -R 0777 $SECNET_PATH/sys/conf

#copy over main.cf and master.cf postfix conf files
cp $SECNET_CONF/postfix/main.cf $SECNET_PATH/sys/conf/postfix
cp $SECNET_CONF/postfix/master.cf $SECNET_PATH/sys/conf/postfix

chmod 755 $SECNET_PATH/sys/conf/
chmod 755 $SECNET_PATH/sys/conf/postfix
chmod 755 $SECNET_PATH/sys/conf/postfix/main.cf
chmod 755 $SECNET_PATH/sys/conf/postfix/master.cf

cd $SECNET_PATH/src/postfix-2.3.13

make tidy # if you have left-over files from a previous build

#-DHAS_SSL
#-R/secnet/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib #this is for Solaris
#/var/lib:/lib:/usr/lib:/usr/local/lib
#make makefiles CCARGS='-DDEF_CONFIG_DIR=\"/secnet/sys/conf/postfix\" -DUSE_TLS -I/secnet/sys/openssl/include' AUXLIBS="-L/secnet/sys/openssl/lib -lssl -lcrypto" 

make makefiles CCARGS='-DDEF_CONFIG_DIR=\"/secnet/sys/conf/postfix\" -DUSE_TLS' AUXLIBS="-lssl -lcrypto" 

make

make upgrade
#make install

#$SECNET_PATH/sys/conf/postfix/post-install command_directory=/secnet/sys/postfix/command upgrade-source

# create and chmod special logs dir for postfix
#mkdir $SECNET_PATH/work/postfix
mkdir $SECNET_PATH/work/postfix/logs
chown secnetu $SECNET_PATH/work/postfix/logs
chgrp postdrop $SECNET_PATH/work/postfix/logs
chmod 1770 $SECNET_PATH/work/postfix/logs

# create and chmod special mail delivery dir for postfix
mkdir $SECNET_PATH/work/mail
chown secnetu $SECNET_PATH/work/mail
chgrp postdrop $SECNET_PATH/work/mail
chmod 0777 $SECNET_PATH/work/mail

# redirect postfix logging to purpose created dir
## This should be changed to something that edits other entries out of the syslog.conf file 
## or source should be edited to not use syslog,
## or just use syslog on a hardened and tripwired box
echo "mail.* -/secnet/work/postfix/logs/mail.log" >> /etc/rsyslog.conf
#restart syslog service here
service rsyslog restart
/sbin/service rsyslog restart

## Then you can start postfix here
# server will be started by start script
#$SECNET_PATH/sys/postfix/command/postfix start

