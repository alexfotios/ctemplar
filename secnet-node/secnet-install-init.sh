## TODO
## This should run on a minimal services hardened linux box
## In last part of installation build sources should be completely removed
## Use ldd to verify linking with proper secnet subsystem libs
#############################################################

## set secnet node name interactively


## setup host resolution in the host file
## a bunch of private addresses that correspond to certain secnet node names

## stuff is copied from secnet inastallation medium into the local src dir and then
## processed/compiled there (for faster access) as needed	

## start executing the following as root

## setup secnetu account
/usr/sbin/groupadd secnetu
groupadd secnetu
/usr/sbin/useradd -g secnetu secnetu
useradd -g secnetu secnetu
echo secnetu | passwd --stdin secnetu

## setup secnet user accounts
/usr/sbin/groupadd marcelo
groupadd marcelo
/usr/sbin/useradd -g marcelo marcelo
useradd -g marcelo marcelo
echo marcelo | passwd --stdin marcelo

## setup secnet user accounts
/usr/sbin/groupadd carlo
groupadd carlo
/usr/sbin/useradd -g carlo carlo
useradd -g carlo carlo
echo carlo | passwd --stdin carlo

/usr/sbin/groupadd fotios2
groupadd fotios2
/usr/sbin/useradd -g fotios2 fotios2
useradd -g fotios2 fotios2
echo fotios2 | passwd --stdin fotios2

## setup mail filter account
/usr/sbin/groupadd secnetf
groupadd secnetf
/usr/sbin/useradd -g secnetf secnetf
useradd -g secnetf secnetf
echo secnetf | passwd --stdin secnetf

#./secnet-setup-hostname.sh

## setup SecNet dir tree
mkdir $SECNET_PATH
mkdir $SECNET_PATH/src
mkdir $SECNET_PATH/sys
mkdir $SECNET_PATH/work

# setup mail filter spool dir
mkdir $SECNET_PATH/work/filter-spool
chown secnetf:secnetf $SECNET_PATH/work/filter-spool
chmod 0700 $SECNET_PATH/work/filter-spool

## other dir permission stuff
chmod 0777 $SECNET_PATH
chmod 0777 $SECNET_PATH/work
 
# now copy cgi, filter and html sources
# SOURCES_PATH should eventually contain the cgi sources subdir as well?
# so the following 2 lines will eventually be removed?
mkdir $SECNET_PATH/src/cgi
cp $CGI_SOURCES_PATH/*.c $SECNET_PATH/src/cgi

mkdir $SECNET_PATH/src/mail-filter
cp $MAIL_FILTER_SOURCES_PATH/tlpf.c $SECNET_PATH/src/mail-filter
cp $MAIL_FILTER_SOURCES_PATH/secnetf.sh $SECNET_PATH/src/mail-filter

mkdir $SECNET_PATH/src/html
mkdir $SECNET_PATH/src/html/snippets
mkdir $SECNET_PATH/src/html/xml
cp $HTML_SOURCES_PATH/snippets/*.htm $SECNET_PATH/src/html/snippets
cp $HTML_SOURCES_PATH/xml/*.xml $SECNET_PATH/src/html/xml
cp $HTML_SOURCES_PATH/xml/*.xsl $SECNET_PATH/src/html/xml

cp -R $SOURCES_PATH/* $SECNET_PATH/src
#expand all tarballs from $SOURCES_PATH to $SECNET_PATH/src
#PATHS=`find $SOURCES_PATH -name "*gz"`
PATHS=`ls $SOURCES_PATH/*gz`
cd $SECNET_PATH/src  #first cd to the dir where the tarballs will be extracted to
for tarball in $PATHS; do tar -xzf $tarball; done #untar each file using shell loop
