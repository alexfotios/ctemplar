## Step 2: install & configure mySQL
cd $SECNET_PATH/src/mysql-5.0.22
/usr/sbin/groupadd secnetu
groupadd secnetu
/usr/sbin/useradd -g secnetu secnetu --password ludlow90
useradd -g secnetu secnetu --password ludlow90
mkdir $SECNET_PATH/work/mysql
mkdir $SECNET_PATH/work/mysql/var
chown -R secnetu $SECNET_PATH/work
chgrp -R secnetu $SECNET_PATH
chmod -R 1770 $SECNET_PATH/work/*
chmod 0777 $SECNET_PATH/work
CFLAGS="-O3" CXX=gcc CXXFLAGS="-O3 -felide-constructors -fno-exceptions -fno-rtti" ./configure --prefix=$SECNET_PATH/sys/mysql --enable-assembler --with-openssl=$SECNET_PATH/sys/openssl
export LD_LIBRARY_PATH="$SECNET_PATH/sys/openssl/lib:$LD_LIBRARY_PATH"
echo "the lib path is: $LD_LIBRARY_PATH"
make
make install
cd $SECNET_PATH/sys/mysql
bin/mysql_install_db --user=secnetu --ldata=$SECNET_PATH/work/mysql/var
chown -R root .
chgrp -R secnetu .
mkdir $SECNET_PATH/work/mysql/var/tmp
chown -R secnetu $SECNET_PATH/work
chgrp -R secnetu $SECNET_PATH
chmod -R 1770 $SECNET_PATH/work/*
chmod 0777 $SECNET_PATH/work
bin/mysqld_safe --no-defaults --basedir=$SECNET_PATH/sys/mysql --datadir=$SECNET_PATH/work/mysql/var --log-error=$SECNET_PATH/work/mysql/var/errorlog --port=3306 --tmpdir=$SECNET_PATH/work/mysql/var/tmp --socket=$SECNET_PATH/work/mysql/var/mysql.sock --user=secnetu &
#SET A PASSWORD FOR THE MySQL root USER
$SECNET_PATH/sys/mysql/bin/mysqladmin --no-defaults --socket=/secnet/work/mysql/var/mysql.sock -u root password 'ludlow90'
$SECNET_PATH/sys/mysql/bin/mysqladmin --no-defaults --socket=/secnet/work/mysql/var/mysql.sock -u root -h ascalon.cs.jrc.it password 'ludlow90'
$SECNET_PATH/sys/mysql/bin/mysqladmin --no-defaults --socket=/secnet/work/mysql/var/mysql.sock -u secnetu password 'ludlow90'
$SECNET_PATH/sys/mysql/bin/mysqladmin --no-defaults --socket=/secnet/work/mysql/var/mysql.sock -u secnetu -h ascalon.cs.jrc.it password 'ludlow90'

######################
##The following tests fail due to issues with the compilation of perl's DBD::mysql client libs (needs manual tweaking of make params)
##For cpan based installation, ncftp client intallation is also required
#cd $SECNET_PATH/sys/mysql/sql-bench ; perl run-all-tests --server=mysql --cmp=mysql,pg,solid --user=secnetu --password=ludlow90 --log
######################
