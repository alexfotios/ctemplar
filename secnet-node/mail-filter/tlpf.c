#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//inline int myExit(int exitValue) __attribute__((always_inline));

char* substr(char* s, char* startStr, char* endStr, int strSize)
{
    char* start = strstr(s, startStr);
    char* end = strstr(start+1, endStr);
    
    char* c;
    char* ss = (char*) malloc(strSize);

    c = start;
    while (c != end) *(ss+(c-start)) = *c++;
    *(ss+(c-start)) = '\0';
    
    return ss;
}

int sCount;

char** split(char* s, char* delimiters)
{   
    char** sa = (char**) malloc(1); sa[0] = NULL;
    
    char* s2 = (char*) malloc(strlen(s));
    strcpy(s2, s);
    
    int i = 0;        
    char *t = strtok(s2, delimiters);
    
    while (t)
    {
        sa = (char**) realloc(sa,i+1);
        *(sa+i) = (char*) malloc(strlen(t));
        *(sa+i+1) = 0;
        
        strcpy(*(sa+i++), t);
        
        t = strtok(NULL, delimiters);
    }

    sCount = i;
    
    free(s2);
    
    return sa;
}

//finds length of null terminated array of strings
int salen (char** sa)
{
    int i = 0;
    char** p = sa;
    while(*p++) i++;
    return i;
}

//splits on a number of delimiting chars (supplied as a string of chars)
char** split3(char* s, char* d)
{
    char **tokens = (char**) malloc(512); tokens[0] = NULL;
    int tokenSize = 1024;
       
    char* t = strtok(s, d);
    
    int i = 0;
    
    while (t)
    {        
        tokens[i] = (char*) malloc(tokenSize);
        tokens[i+1] = NULL;      
        strcpy(tokens[i++], t);               
        t = strtok(NULL, d);
    }
    
    return tokens;
}


int isKnownDomain(char* domain)
{
    // this will later one check an actual table of known orgs that may be dynamically updated
    // for now however it simply does a quick inline check for the two test orgs of this demo: cspc56 and cspc84
    
    if (!strcmp(domain, "node1.telecom.it") || !strcmp(domain, "node1.enercom.de")) return 1;
    
    return 0;
}

#define WHITE 1
#define GREEN 2
#define AMBER 3
#define RED   4
int getTLPC(char* sColor)
{
    if (!strcmp(sColor, "white")) return WHITE;
    else if (!strcmp(sColor, "green")) return GREEN;
    else if (!strcmp(sColor, "amber")) return AMBER;
    else if (!strcmp(sColor, "red")) return RED;
    else return 0; //unknown color
}


char *msg = NULL, *headers = NULL, *ToHeader = NULL, *FromHeader = NULL, *TLPHeader = NULL;
char **temp = NULL, **recipients = NULL;

int main(int* argc, char** argv)
{
    //return myExit(1);
    
    int tlpc; //treating tlp color as an int instead of a string will make the filter faster
    
    //alocate msg buffer
    msg = (char*) malloc(65536); *msg = 0;
    
    //allocate line buffer
    char* s = (char*) malloc(1024);

    //read in message from stdin, for now ANSI only, 64K max
    while (fgets(s, 1024, stdin))
    {
        strcat(msg, s);
    }
    
    free(s);
    
    //printf("%s", msg); //debug
    //return 0;
    
    
	//first find headers section
    char* start;
    char* end = strstr(msg, "\n\n");
    
    //headers = (char*) malloc(65536);
    
    headers = substr(msg, "", "\n\n", 65536); 
    strcat(headers, "\n"); 
    
    //printf("%s", headers); //debug
    //return 0;

	//then get the to:, from: and TLP headers
    //ToHeader = (char*) malloc(1024);
    //FromHeader = (char*) malloc(1024);
    //TLPHeader = (char*) malloc(1024);

    ToHeader = substr(headers, "\nTo:", "\n", 1024) + 5;
    FromHeader = substr(headers, "\nFrom:", "\n", 1024) + 7;
    TLPHeader = substr(headers, "\nX-SecNetIE-TLP:", "\n", 1024) + 17;
    
    tlpc = getTLPC(TLPHeader);
    
    //printf("check: %d\n", tlpc); //debug
    //return 0;    

	//split the from header
    temp = split3(FromHeader, ";");    
    sCount = salen(temp);
	
    //and if more than one mail originator reject the mail
    if (sCount != 1) return myExit(1);
    
    //freeStringArray(temp);

	//split the to header and count number of recipients
    recipients = split3(ToHeader, ";"); //from here on, number of recipients is sCount
    sCount = salen(recipients);
    
    //printf("%d\n", sCount); //debug
    //return 0;        
    
    //here we can possibly impose an overall max number of recipients limit
    
    //////////////////////////////////
    //from here on, call an exit funtion that returns proper value and frees allocated memory
    
    //then a big if/then/else statement dealing with each possible TLP color
    if (tlpc == WHITE){
        return myExit(0); //no restrictions for white
    }   
    else if (tlpc == GREEN){        
        //check that all recipient domains are in the known orgs table (later)        
        int i;
        for (i = 0; i < sCount; i++){
            if (!isKnownDomain(strchr(recipients[i],'@')+1)) return myExit(4);
        }
        
        //if all known then return 0
        return myExit(0);
    }
    else if (tlpc == AMBER){
        //check that all recipient domains are the same as the originator's
        char* FromDomain = strchr(FromHeader,'@')+1;
        
        int i;
        for (i = 0; i < sCount; i++){
            char* d = strchr(recipients[i],'@');
            if (!d) return myExit(6);
            if (strcmp(d+1, FromDomain)) return myExit(5);
        }        
        
        //if they are the same, then return 0
        return myExit(0);
    }
    else if (tlpc == RED){
        //if number of recipients is more than one, reject it
        if (sCount > 1) return myExit(7);
        
        //if message is being forwarded, reject it (later)
        
        //also check if signature of originator org's auditor is present (later)
        
        return myExit(0);
    }
    else 
        return myExit(2);
    
    return myExit(3); //rejection of unknown reason
} 

//will free the memory allocated to a NULL teminated array of strings
void freeStringArray(char** sa)
{
    int i = 0;
    char* temp;
    
    while (temp = *(sa+i++)) free(temp);
    
    free(sa);
}

int myExit(int exitValue) //implement it as inline function/macro (later)
{
    
    if (msg) free(msg);
    if (headers) free(headers);
    
    if (ToHeader) free(ToHeader-5);
    if (FromHeader) free(FromHeader-7);
    if (TLPHeader) free(TLPHeader-17);
    
    if (temp) freeStringArray(temp);
    if (recipients) freeStringArray(recipients);
    
    return exitValue;
}


