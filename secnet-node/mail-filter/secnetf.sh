#!/bin/bash
# Simple shell-based filter. It is meant to be invoked as follows:
#       /path/to/script -f sender recipients...

export SECNET_PATH="/secnet"

# Localize these. The -G option does nothing before Postfix 2.3.
INSPECT_DIR="$SECNET_PATH/work/filter-spool"
SENDMAIL="$SECNET_PATH/sys/postfix/bin/sendmail -G -i" # NEVER NEVER NEVER use "-t" here.

# Exit codes from <sysexits.h>
EX_TEMPFAIL=75
EX_UNAVAILABLE=69

# Clean up when done or when aborting.
trap "rm -f in.$$" 0 1 2 3 4 5 6 7 15

# Start processing.
cd $INSPECT_DIR || {
     echo $INSPECT_DIR does not exist; exit $EX_TEMPFAIL; }

cat >in.$$ || { 
	echo Cannot save mail to file; exit $EX_TEMPFAIL; }

# Specify your content filter here.
$SECNET_PATH/sys/filter/tlpf <in.$$ || {
	echo Message content rejected; exit $EX_UNAVAILABLE; }

$SENDMAIL "$@" <in.$$

exit $?
