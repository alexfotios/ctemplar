export SECNET_PATH="/secnet"

#make $SECNET_PATH/sys/filter/ dir
#set permissions to secnetf only
mkdir $SECNET_PATH/sys/filter
chown secnetf:secnetf $SECNET_PATH/sys/filter
chmod 0700 $SECNET_PATH/sys/filter

# compile tlpf
gcc $SECNET_PATH/src/mail-filter/tlpf.c -o $SECNET_PATH/src/mail-filter/tlpf

# then copy the binary of tlpf to $SECNET_PATH/sys/filter/
cp $SECNET_PATH/src/mail-filter/tlpf $SECNET_PATH/sys/filter/

# set its ownership and permissions (secnetf:secnetf, 0700)
chown secnetf:secnetf $SECNET_PATH/sys/filter/tlpf
chmod 0700 $SECNET_PATH/sys/filter/tlpf

# also copy there the secnetf.sh file
cp $SECNET_PATH/src/mail-filter/secnetf.sh $SECNET_PATH/sys/filter/

# set its ownership and permissions (secnetf:secnetf, 0700)
chown secnetf:secnetf $SECNET_PATH/sys/filter/secnetf.sh
chmod 0700 $SECNET_PATH/sys/filter/secnetf.sh

# make sure secnetf user has execute access to the /secnet/sys/postfix/bin dir
chmod +x /secnet/sys/postfix/bin
