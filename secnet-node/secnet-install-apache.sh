
#mkdir $SECNET_PATH/sys/apache

cd $SECNET_PATH/src/httpd-2.2.4

make clean
make tidy

# Some influential environment variables:
#   CC          C compiler command
#   CFLAGS      C compiler flags
#   LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
#               nonstandard directory <lib dir>
#   CPPFLAGS    C/C++ preprocessor flags, e.g. -I<include dir> if you have
#               headers in a nonstandard directory <include dir>
#   CPP         C preprocessor
CPPFLAGS="-I$SECNET_PATH/sys/openssl/include" LDFLAGS="-L$SECNET_PATH/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib" ./configure --prefix=$SECNET_PATH/sys/apache --enable-ssl --enable-cgi --enable-cgid --with-ssl=$SECNET_PATH/sys/openssl
make
make install

#copy the config files over (delete the default ones)
rm -f $SECNET_PATH/sys/apache/conf/httpd.conf
rm -f $SECNET_PATH/sys/apache/conf/extra/httpd.conf
cd $SECNET_INSTALL_DIR
cp -f $SECNET_CONF/apache/httpd.conf $SECNET_PATH/sys/apache/conf
cp -f $SECNET_CONF/apache/httpd-ssl.conf $SECNET_PATH/sys/apache/conf/extra

#copy the apache SSL cert over (doe it have the key combined?)
cp -f $SECNET_INSTALL_DIR/certs/apache/secnet-apache.crt $SECNET_PATH/sys/apache/conf
cp -f $SECNET_INSTALL_DIR/certs/apache/secnet-apache.key $SECNET_PATH/sys/apache/conf

# server will be started by start script
#$SECNET_PATH/sys/apache/bin/httpd -f$SECNET_PATH/sys/apache/conf/httpd.conf -k start
