-------------------------------------------------------------------------------
-- README
--  Display XML Document as Tree on Web
-- MODIFIED (MM/DD/YYYY)
--  jiwang   07/29/2003 - Add the content branch to the tree.
--  jiwang   07/01/2003 - Clean up for OTN Tutorial
--  jiwang   02/15/2003 - Create
-------------------------------------------------------------------------------
The sample using recursive XSLT calls to display XML document in tree views. 
This helps to demostrate XML structure to the customer and is used in XML 
Semantic Analysis Demonstration.

Key Points
==========
1. How to write recursive call using XSL Languages
2. Javascript programming and customization using XSLT

XSLT Template for Tree view
===========================
  <xsl:template name="treeview">
    <xsl:param name="depth"/>
    <xsl:param name="content"/>
    <xsl:choose>
      <!-- First Level: Generate the ROOT of the Tree -->
      <xsl:when test="number($depth)=0">
          foldersTree = gFld("Document Tree View", "")
            <xsl:for-each select="$content/*">
          <xsl:call-template name="treeview">
            <xsl:with-param name="content" select="."/>
            <xsl:with-param name="depth" select="number($depth)+1"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$content/*">
            <xsl:choose>
              <xsl:when test="number($depth)=1">
            aux<xsl:value-of select="$depth"/> = 
                  insFld(foldersTree, gFld("<xsl:value-of select="name(.)"/>", ""))
              </xsl:when>
              <xsl:otherwise>
            aux<xsl:value-of select="$depth"/> = 
                insFld(aux<xsl:value-of select="number($depth)-1"/>, 
                       gFld("<xsl:value-of select="name(.)"/>", 
                       ""<xsl:if test="@isPicked">,<xsl:value-of select="@isPicked"/></xsl:if>))
              </xsl:otherwise>
            </xsl:choose>
            <xsl:for-each select="$content/*">
              <xsl:call-template name="treeview">
                <xsl:with-param name="content" select="."/>
                <xsl:with-param name="depth" select="number($depth)+1"/>
              </xsl:call-template>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            insDoc(aux<xsl:value-of select="number($depth)-1"/>, 
                   gLnk(1, "<xsl:value-of select="name(.)"/>", ""))
        </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
--------------------------------- End of File ---------------------------------
