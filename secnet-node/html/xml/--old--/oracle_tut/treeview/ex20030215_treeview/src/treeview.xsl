<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes"/>
  <xsl:template match="/">
    <html>
      <body onResize="if (avigator.family == 'nn4') window.location.reload()">
        <script src="js/treemenu.js"/>
        <script>
          
		  <xsl:call-template name="treeview">
            <xsl:with-param name="content" select="/"/>
            <xsl:with-param name="depth" select="0"/>
          </xsl:call-template>
		  
        initializeDocument();
       </script>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template name="treeview">
    <xsl:param name="depth"/>
    <xsl:param name="content"/>
    <xsl:choose>
      <xsl:when test="number($depth)=0">
          foldersTree = gFld("Document Tree View", "")
         <xsl:for-each select="$content/*">
          <xsl:call-template name="treeview">
            <xsl:with-param name="content" select="."/>
            <xsl:with-param name="depth" select="number($depth)+1"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$content/*">
            <xsl:choose>
              <xsl:when test="number($depth)=1">
            aux<xsl:value-of select="$depth"/> = insFld(foldersTree, gFld("<xsl:value-of select="name(.)"/>", ""))
              </xsl:when>
              <xsl:otherwise>
            aux<xsl:value-of select="$depth"/> = 
                insFld(aux<xsl:value-of select="number($depth)-1"/>, gFld("<xsl:value-of select="name(.)"/>", ""<xsl:if test="@isPicked">,<xsl:value-of select="@isPicked"/></xsl:if>))
              </xsl:otherwise>
            </xsl:choose>
            <xsl:for-each select="$content/*">
              <xsl:call-template name="treeview">
                <xsl:with-param name="content" select="."/>
                <xsl:with-param name="depth" select="number($depth)+1"/>
              </xsl:call-template>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            aux<xsl:value-of select="$depth"/> = insFld(aux<xsl:value-of select="number($depth)-1"/>, gFld("<xsl:value-of select="name(.)"/>", ""))
            insDoc(aux<xsl:value-of select="number($depth)"/>, gLnk(1, "<xsl:value-of select="."/>", ""))
        </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
