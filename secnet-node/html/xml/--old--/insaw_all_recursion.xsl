<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
<xsl:template name="processNode">
	<xsl:param name="count"/>
	
    <div class="elementDiv"> 		
		<xsl:attribute name="id">
			<xsl:value-of select="concat('element',string($count))"/>
		</xsl:attribute>
	</div>
	-->
	
	<!--	
	
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>
                <td width="100%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    					<tr>
            				<td nowrap="true" xid="flag1" class="flagCell" onclick="fold('element1')"></td>								
							<td> </td>                    
                        	<td xid="title1" class="titleCell" nowrap="true">
								<xsl:value-of select="@name"/>
							</td>
                        	<td> </td>
                        	<td xid="description2" class="descriptionCell">
								<xsl:value-of select="@description"/>
							</td>
                        	<td width="100%"> </td>
    					</tr>                            
                    </table>
                </td>
            </tr>							
                    
            <tr>
                <td class="contentCell" xid="content1" width="100%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
							<td> </td>
						</tr>
                        <tr>
                            <td></td>                    
             				<td width="100%">
							-->
				
            		            <!-- content start -->	
	<!--
                            	<xsl:if test="$node/node">
                            		<xsl:call-template name="processNode">
                            			<xsl:with-param name="count" select="$count + 1"/>
										<xsl:with-param name="count" select="$node/node"/>
                            		</xsl:call-template>
                            	</xsl:if>
							
        	            	</td>
                    	</tr>
                        <tr>
							<td> </td>
						</tr>
                   	</table>
              	</td>					
           	</tr>
      	</table>
    </div>									

	
</xsl:template>
-->

    <xsl:template name="processNode">
    	<xsl:param name="nodelist"/>
    	<xsl:param name="count" select="0"/>
			
    		<xsl:if test="$nodelist[$count]/*">	
    			<xsl:call-template name="processNode">
    				<xsl:with-param name="nodelist" select="$nodelist[$count]/*"/>
    				<xsl:with-param name="count" select="$count + 1"/>
    			</xsl:call-template>
    		</xsl:when>
			
			<!--
			<xsl:when test="$nodeList[$count]/*">
    			<xsl:call-template name="processNode">
    				<xsl:with-param name="nodeList" select="$nodeList[$count]/*"/>
    				<xsl:with-param name="count" select="$count + 1"/>
    			</xsl:call-template>			
			</xsl:when>
			-->
			
			<xsl:otherwise>
			</xsl:otherwise>
			
    	</xsl:choose>

    </xsl:template>
		
    <xsl:template match="/">
		<xsl:call-template name="processNode">
			<xsl:with-param name="nodelist" select="/*"/>
		</xsl:call-template>
	</xsl:template>

</xsl:stylesheet>