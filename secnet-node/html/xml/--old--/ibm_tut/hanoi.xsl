<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xslt">
    
    <xsl:output method="xml"/>
    
    <xsl:template match="/">
    	<xsl:variable name="size" select="number(/*/Hanoi/@Size)"/>
    	<HanoiSolution Size="{$size}">
    		<xsl:call-template name="hanoi">
    			<xsl:with-param name="height"    select="$size"/>
    			<xsl:with-param name="fromTower" select="'A'"/>
    			<xsl:with-param name="toTower"   select="'B'"/>
    			<xsl:with-param name="withTower" select="'C'"/>
    		</xsl:call-template>
    	</HanoiSolution>
    </xsl:template>
    
    <xsl:template name="hanoi">
    	<xsl:param name="height"/>
    	<xsl:param name="fromTower"/>
    	<xsl:param name="toTower"/>
    	<xsl:param name="withTower"/>
    	
    	<xsl:if test="$height &gt;= 1">
    		<xsl:call-template name="hanoi">
    			<xsl:with-param name="height"    select="$height - 1"/>
    			<xsl:with-param name="fromTower" select="$fromTower"/>
    			<xsl:with-param name="toTower"   select="$withTower"/>
    			<xsl:with-param name="withTower" select="$toTower"/>
    		</xsl:call-template>
    		<Move FromTower="{$fromTower}" ToTower="{$toTower}"/>
    		<xsl:call-template name="hanoi">
    			<xsl:with-param name="height"    select="$height - 1"/>
    			<xsl:with-param name="fromTower" select="$withTower"/>
    			<xsl:with-param name="toTower"   select="$toTower"/>
    			<xsl:with-param name="withTower" select="$fromTower"/>
    		</xsl:call-template>
    	</xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
