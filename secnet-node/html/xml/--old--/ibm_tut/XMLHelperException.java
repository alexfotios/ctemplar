import java.io.*;

import org.xml.sax.*;
import javax.xml.transform.*;

/**
 * An exception class that wraps any other exception that may be thrown within
 * a method inside the XMLHelper class. The getMessage() and printStackTrace()
 * methods will include the information from the inner exception wrapped by this
 * class.
 *
 * @author <a href="mailto:jjared@almaden.ibm.com">Jared Jackson</a>
 */

public class XMLHelperException extends Exception {
	private Exception innerException;
        
        /**
         * Default constructor. Has no inner exception.
         */
        
	public XMLHelperException() { super(); innerException = null; }
        
        /**
         * Message constructor. Has no inner exception.
         */
        
	public XMLHelperException(String message) { super(message); innerException = null; }
        
        /**
         * Inner exception constructor. Uses the message provided as well as that contained
         * in the inner exception.
         */
        
	public XMLHelperException(String message, Exception innerException) {
		super(message);
		this.innerException = innerException;
	}
        
        /**
         * Returns the message associated with this exception which includes within it the
         * message from the inner exception if that inner exception exists.
         */
	
	public String getMessage() {
		String message = super.getMessage() + "\n" + 
			   (innerException == null ? "" : innerException.getMessage());
		if (innerException instanceof SAXParseException) {
                    SAXParseException ex = (SAXParseException)innerException;
                    message += "\n** Line " + ex.getLineNumber() + " **";
                } else if (innerException instanceof TransformerException) {
                    TransformerException ex = (TransformerException)innerException;
                    message = ex.getMessageAndLocation();
                }
		//super.printStackTrace(System.err);
		return message;
	}
	
        /**
         * Prints the stack trace including the trace of the inner exception if it exists
         */
        
	public void printStackTrace(PrintStream ps) {
		super.printStackTrace(ps);
		innerException.printStackTrace(ps);
	}
}
