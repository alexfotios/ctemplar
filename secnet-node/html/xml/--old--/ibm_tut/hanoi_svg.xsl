<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xslt"
    xmlns:xlink="http://www.w3.org/1999/xlink">
    
    <xsl:output method="html"/>
    
    <xsl:variable name="towerHeight" select="50"/>
    <xsl:variable name="towerStackHeight" select="40"/>
    <xsl:variable name="towerWidth" select="5"/>
    <xsl:variable name="baseHeight" select="6"/>
    <xsl:variable name="baseWidth" select="30"/>
    <xsl:variable name="smallDiskWidth" select="10"/>
    <xsl:variable name="offset" select="15"/>
    
    <xsl:variable name="width" select="(4 * $offset) + (3 * $baseWidth)"/>
    <xsl:variable name="stepHeight" select="2 * $offset + $towerHeight + $baseHeight"/>
    
    <xsl:variable name="initSize" select="number(/*/@Size)"/>
    <xsl:variable name="moves" select="/*/Move"/>
    <xsl:variable name="numMoves" select="count($moves)"/>
    <xsl:variable name="height" select="($numMoves + 1) * $stepHeight"/>
    <xsl:variable name="diskHeight" select="$towerStackHeight div $initSize"/>
    
    
    <xsl:template match="/">
    	<xsl:variable name="startingTower">
    		<xsl:call-template name="initTower">
    			<xsl:with-param name="towerName" select="'A'"/>
    			<xsl:with-param name="size" select="$initSize"/>
    		</xsl:call-template>
    	</xsl:variable>
    	
    	<svg width="{$width}px" height="{$height}px" viewBox="0 0 {$width} {$height}">
    	
    		<defs>
    			<g id="tower">
    				<xsl:variable name="x1" select="($baseWidth - $towerWidth) div 2"/>
    				<xsl:variable name="x2" select="$baseWidth div 2"/>
    				<xsl:variable name="x3" select="($baseWidth + $towerWidth) div 2"/>
    				<xsl:variable name="y1" select="0"/>
    				<xsl:variable name="y2" select="$towerHeight - $towerStackHeight"/>
    				<xsl:variable name="y3" select="$towerHeight"/>
    				<path fill="white" stroke="black" d="M {$x1},{$y3} L {$x1},{$y2} L {$x2},{$y1} L {$x3},{$y2} L {$x3},{$y3}"/>
    				<rect x="0" y="{$y3}" rx="2" ry="2" width="{$baseWidth}" height="{$baseHeight}" stroke="black" fill="#444444"/>
    			</g>
    			<g id="tower_step">
    				<g transform="translate({$offset},{$offset})">
	    				<use xlink:href="#tower"/>
	    			</g>
	    			<g transform="translate({(2 * $offset) + $baseWidth },{$offset})">
	    				<use xlink:href="#tower"/>
	    			</g>
	    			<g transform="translate({(3 * $offset) + (2 * $baseWidth)},{$offset})">
	    				<use xlink:href="#tower"/>
	    			</g>
    			</g>
    		</defs>
    		
    		<xsl:call-template name="drawMove">
    			<xsl:with-param name="moves" select="$moves"/>
    			<xsl:with-param name="towerA" select="$startingTower"/>
    			<xsl:with-param name="towerB" select="'B'"/>
    			<xsl:with-param name="towerC" select="'C'"/>
    		</xsl:call-template>
    	</svg>
    </xsl:template>
    
    <xsl:template name="drawMove">
    	<xsl:param name="moves"/>
    	<xsl:param name="towerA"/>
    	<xsl:param name="towerB"/>
    	<xsl:param name="towerC"/>
    	<xsl:param name="moveNumber" select="0"/>
    	
    	<xsl:variable name="yOff" select="$moveNumber * $stepHeight"/>
    	
    	
    	<g transform="translate(0,{$yOff})">
    		<use xlink:href="#tower_step"/>
    		
    		<xsl:call-template name="drawTower">
	    		<xsl:with-param name="tower" select="$towerA"/>
	    		<xsl:with-param name="xOff" select="$offset"/>
	    	</xsl:call-template>
	    	
	    	<xsl:call-template name="drawTower">
	    		<xsl:with-param name="tower" select="$towerB"/>
	    		<xsl:with-param name="xOff" select="(2 * $offset) + $baseWidth"/>
	    	</xsl:call-template>
	    	
	    	<xsl:call-template name="drawTower">
	    		<xsl:with-param name="tower" select="$towerC"/>
	    		<xsl:with-param name="xOff" select="(3 *$offset) + (2 * $baseWidth)"/>
	    	</xsl:call-template>
	    	
	    	<text fill="navy" stroke="none" x="7" y="{$offset + 5}"><xsl:value-of select="$moveNumber + 1"/>.</text>
    	</g>
    	
    	<xsl:if test="string-length($towerA) &gt; 1 or $moveNumber = 0 or (string-length($towerB) &gt; 1 and string-length($towerC) &gt; 1)">
    		<xsl:variable name="text" select="concat('From Tower ',concat($moves[1]/@FromTower,concat(' to Tower ',$moves[1]/@ToTower)))"/>
    		<g transform="translate(0,{$yOff})">
    			<text x="{$width div 2}" y="{$stepHeight + 5}" text-anchor="middle" fill="black" stroke="none"><xsl:value-of select="$text"/></text>
    		</g>
    		<xsl:variable name="movedDisk">
	    		<xsl:choose>
	    			<xsl:when test="$moves[1]/@FromTower = 'A'">
	    				<xsl:value-of select="substring($towerA,string-length($towerA) - 1,string-length($towerA))"/>
	    			</xsl:when>
	    			<xsl:when test="$moves[1]/@FromTower = 'B'">
	    				<xsl:value-of select="substring($towerB,string-length($towerB) - 1,string-length($towerB))"/>
	    			</xsl:when>
	    			<xsl:when test="$moves[1]/@FromTower = 'C'">
	    				<xsl:value-of select="substring($towerC,string-length($towerC) - 1,string-length($towerC))"/>
	    			</xsl:when>
	    		</xsl:choose>
	    	</xsl:variable>
	    	<xsl:comment>Moving <xsl:value-of select="$movedDisk"/> From <xsl:value-of select="$moves[1]/@FromTower"/> To <xsl:value-of select="$moves[1]/@ToTower"/></xsl:comment>
	    	<xsl:variable name="newTowerA">
	    		<xsl:choose>
	    			<xsl:when test="$moves[1]/@FromTower = 'A'">
	    				<xsl:value-of select="substring($towerA,0,string-length($towerA) - 1)"/>
	    			</xsl:when>
	    			<xsl:when test="$moves[1]/@ToTower = 'A'">
	    				<xsl:value-of select="concat($towerA,$movedDisk)"/>
	    			</xsl:when>
	    			<xsl:otherwise>
	    				<xsl:value-of select="$towerA"/>
	    			</xsl:otherwise>
	    		</xsl:choose>
	    	</xsl:variable>
	    	<xsl:variable name="newTowerB">
	    		<xsl:choose>
	    			<xsl:when test="$moves[1]/@FromTower = 'B'">
	    				<xsl:value-of select="substring($towerB,0,string-length($towerB) - 1)"/>
	    			</xsl:when>
	    			<xsl:when test="$moves[1]/@ToTower = 'B'">
	    				<xsl:value-of select="concat($towerB,$movedDisk)"/>
	    			</xsl:when>
	    			<xsl:otherwise>
	    				<xsl:value-of select="$towerB"/>
	    			</xsl:otherwise>
	    		</xsl:choose>
	    	</xsl:variable>
	    	<xsl:variable name="newTowerC">
	    		<xsl:choose>
	    			<xsl:when test="$moves[1]/@FromTower = 'C'">
	    				<xsl:value-of select="substring($towerC,0,string-length($towerC) - 1)"/>
	    			</xsl:when>
	    			<xsl:when test="$moves[1]/@ToTower = 'C'">
	    				<xsl:value-of select="concat($towerC,$movedDisk)"/>
	    			</xsl:when>
	    			<xsl:otherwise>
	    				<xsl:value-of select="$towerC"/>
	    			</xsl:otherwise>
	    		</xsl:choose>
	    	</xsl:variable>
	    	<xsl:comment>A: <xsl:value-of select="$newTowerA"/>, B: <xsl:value-of select="$newTowerB"/>, C: <xsl:value-of select="$newTowerC"/></xsl:comment>
	    	
	    	<xsl:call-template name="drawMove">
	    		<xsl:with-param name="moves" select="$moves[position() &gt; 1]"/>
	    		<xsl:with-param name="towerA" select="$newTowerA"/>
	    		<xsl:with-param name="towerB" select="$newTowerB"/>
	    		<xsl:with-param name="towerC" select="$newTowerC"/>
	    		<xsl:with-param name="moveNumber" select="$moveNumber + 1"/>
	    	</xsl:call-template>
    	</xsl:if>
    	
    	
    </xsl:template>
    
    <xsl:template name="drawTower">
    	<xsl:param name="tower"/>
    	<xsl:param name="xOff"/>
    	
    	<xsl:call-template name="drawDisk">
    		<xsl:with-param name="disks" select="substring($tower,2,string-length($tower))"/>
    		<xsl:with-param name="xOff" select="$xOff"/>
    		<xsl:with-param name="yOff" select="$towerHeight + $offset"/> 
    	</xsl:call-template>
    </xsl:template>
    
    <xsl:template name="drawDisk">
    	<xsl:param name="disks"/>
    	<xsl:param name="xOff"/>
    	<xsl:param name="yOff"/>
    	<xsl:if test="string-length($disks) &gt;= 2">
	    	<xsl:variable name="disk" select="substring($disks,1,2)"/>
	    	<xsl:variable name="diskWidth">
	    		<xsl:call-template name="getDiskWidth">
	    			<xsl:with-param name="diskNum" select="$disk"/>
	    		</xsl:call-template>
	    	</xsl:variable>
	    	<xsl:variable name="diskColor">
	    		<xsl:call-template name="getColor">
	    			<xsl:with-param name="diskNum" select="$disk"/>
	    		</xsl:call-template>
	    	</xsl:variable>
	    	
	    	<rect x="{$xOff + ($baseWidth div 2) - ($diskWidth div 2)}" y="{$yOff - $diskHeight}" width="{$diskWidth}" height="{$diskHeight}" fill="{$diskColor}" stroke="#222222" opacity="0.9"/>
	    	<!--<text fill="black" stroke="none" x="{$xOff}" y="{$yOff}"><xsl:value-of select="$disk"/></text>-->
	    	<xsl:call-template name="drawDisk">
	    		<xsl:with-param name="disks" select="substring($disks,3,string-length($disks))"/>
	    		<xsl:with-param name="xOff" select="$xOff"/>
	    		<xsl:with-param name="yOff" select="$yOff - $diskHeight"/>
	    	</xsl:call-template>
	    </xsl:if>
    </xsl:template>
    
    <xsl:template name="getColor">
    	<xsl:param name="diskNum"/>
    	<xsl:choose>
    		<xsl:when test="number($diskNum) = 1">blue</xsl:when>
    		<xsl:when test="number($diskNum) = 2">green</xsl:when>
    		<xsl:when test="number($diskNum) = 3">red</xsl:when>
    		<xsl:when test="number($diskNum) = 4">navy</xsl:when>
    		<xsl:when test="number($diskNum) = 5">cyan</xsl:when>
    		<xsl:when test="number($diskNum) = 6">orange</xsl:when>
    		<xsl:when test="number($diskNum) = 7">magenta</xsl:when>
    		<xsl:when test="number($diskNum) = 8">yellow</xsl:when>
    		<xsl:otherwise>gray</xsl:otherwise>
    	</xsl:choose>
    	
    </xsl:template>
    
    <xsl:template name="getDiskWidth">
    	<xsl:param name="diskNum"/>
    	<xsl:variable name="range" select="$baseWidth - $smallDiskWidth"/>
    	<xsl:variable name="percent" select="$diskNum div $initSize"/>
    	<xsl:value-of select="($percent * $range) + $smallDiskWidth"/>
    </xsl:template>
    
    <xsl:template name="initTower">
    	<xsl:param name="towerName" select="''"/>
    	<xsl:param name="size"/>
    	
    	<xsl:if test="$size &gt; 0">
	    	<xsl:variable name="recursive_result">
 	  	 		<xsl:call-template name="initTower">
 	  	 			<xsl:with-param name="size" select="$size - 1"/>
 	  	 		</xsl:call-template>
  		  	</xsl:variable>
  		  	<xsl:variable name="number">
  		  		<xsl:choose>
  		  			<xsl:when test="$size &lt; 10">
  		  				<xsl:value-of select="concat('0',$size)"/>
  		  			</xsl:when>
  		  			<xsl:otherwise><xsl:value-of select="string($size)"/></xsl:otherwise>
  		  		</xsl:choose>
  		  	</xsl:variable>
  		  	<xsl:value-of select="concat($towerName,concat($number,$recursive_result))"/>
  		</xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
