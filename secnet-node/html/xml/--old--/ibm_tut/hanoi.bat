java -classpath .;xerces.jar;xalan.jar XMLHelper hanoi.xml hanoi.xsl hanoi_solution.xml
@echo Wrote Tower of Hanoi solution XML: hanoi_solution.xml
java -classpath .;xerces.jar;xalan.jar XMLHelper hanoi_solution.xml hanoi_svg.xsl hanoi.svg
@echo Wrote Tower of Hanoi solution SVG: hanoi.svg