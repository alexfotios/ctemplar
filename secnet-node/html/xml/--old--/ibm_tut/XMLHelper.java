import org.w3c.dom.*;
//import org.w3c.tidy.*;
import org.xml.sax.*;

import org.apache.xerces.dom.*;
import org.apache.xerces.parsers.*;
import org.apache.xalan.serialize.*;
import org.apache.xalan.templates.*;
import org.apache.xalan.processor.*;
import org.apache.xml.serialize.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * A utility class for aiding in all things XML. Methods here include parsing helpers,
 * XML serializing and output, XSL transformation, HTML retrieval and XML mapping, and
 * various document traversal methods. All of the methods are static.
 *
 * @author <a href="mailto:jjared@almaden.ibm.com">Jared Jackson</a>
 * @see XMLHelperException
 */
	
public class XMLHelper {
    
    /**
     * Returns the number of elements the source element has as children
     */
    
    public static int getElementChildCount(Element sourceElem) {
        int result = 0;
        NodeList nl = sourceElem.getChildNodes();
        for (int i=0; i < nl.getLength(); i++) {
            if (nl.item(i) instanceof Element) result++;
        }
        return result;
    }
    
    /**
     * Returns the number of elements the source element has as children
     * restricting the elements to those of a given tag name.
     */
    
    public static int getElementChildCount(Element sourceElem, String tagName) {
        NodeList nl = sourceElem.getElementsByTagName(tagName);
        return nl.getLength();
    }
    
    /**
     * Returns the number of elements the source element has as children
     * restricting the elements to those of a given tag name.
     */
    
    public static int getElementChildCount(Element sourceElem, String namespace, String tagName) {
        NodeList nl = sourceElem.getElementsByTagNameNS(namespace, tagName);
        return nl.getLength();
    }
    
    /**
     * Does the element have children that are also elements?
     */
    
    public static boolean hasChildElement(Element sourceElem) {
        NodeList nl = sourceElem.getChildNodes();
        for (int i=0; i < nl.getLength(); i++) {
            if (nl.item(i) instanceof Element) return true;
        }
        return false;
    }
    
    /**
     * Does the element have element children of a specific tag name?
     */
    
    public static boolean hasChildElement(Element sourceElem, String tagName) {
        return getElementChildCount(sourceElem, tagName) > 0;
    }

    /**
     * Does the element have element children of a specific tag name?
     */
    
    public static boolean hasChildElement(Element sourceElem, String namespace, String tagName) {
        return getElementChildCount(sourceElem, namespace, tagName) > 0;
    }
    
    /**
     * Returns the first child node of an element that is also an element.
     * @param sourceElem The parent XML element being inspected.
     * @return First child element or <code>null</code> if none exists.
     */
    
    public static Element getFirstChildElement(Element sourceElem) {
        NodeList nl = sourceElem.getChildNodes();
        for (int i=0; i < nl.getLength(); i++) {
            org.w3c.dom.Node node = nl.item(i);
            if (node instanceof Element) return (Element)node;
        }
        return null;
    }
    
    /**
     * Returns the first child element of a given name.
     * @param sourceElem The parent XML element being inspected.
     * @param tagName A case sensitive tag name to match children elements to.
     * @result The matched child element or <code>null</code> if none exists.
     */
    
    public static Element getFirstChildElement(Element sourceElem, String tagName) {
        NodeList nl = sourceElem.getElementsByTagName(tagName);
        if (nl.getLength() > 0) return (Element)nl.item(0);
        else return null;
    }
    
    /**
     * Returns the first child element of a given name.
     * @param sourceElem The parent XML element being inspected.
     * @param namespace The namespace URI for the element
     * @param tagName A case sensitive tag name to match children elements to.
     * @result The matched child element or <code>null</code> if none exists.
     */
    
    public static Element getFirstChildElement(Element sourceElem, String namespace, String tagName) {
        NodeList nl = sourceElem.getElementsByTagNameNS(namespace, tagName);
        if (nl.getLength() > 0) return (Element)nl.item(0);
        else return null;
    }
    
    /**
     * Returns the text underneath the parameter element. Recursion automatically
     * occurs, so the entire tree beneath the parameter node is examined for text.
     * The result includes all text whitespace found in the XML source.
     */
    
    public static String getElementText(Element sourceElem) {
        return getElementText(sourceElem, true);
    }
    
    /**
     * Returns the text underneath the parameter element. If recursive is not set,
     * only those text nodes that are direct children of the source node are
     * examined. The result includes all text whitespace found in the XML source.
     */
    
    public static String getElementText(Element sourceElem, boolean recursive) {
        StringBuffer buff = new StringBuffer();
        
        NodeList nl = sourceElem.getChildNodes();
        for (int i=0; i < nl.getLength(); i++) {
            org.w3c.dom.Node node = nl.item(i);
            if (node instanceof Text) {
                String text = node.getNodeValue();
                if (text != null) buff.append(text);
            } else if (recursive && node instanceof Element) {
                String text = getElementText((Element)node, true);
                if (text != null) buff.append(text);
            }
        }
        
        return buff.toString();
    }
    
    /**
     * Creates a new XML document with the root element given the tag name
     * specified in the parameter. Don't ask me why I didn't call this createXML().
     * I should have. This version remains in the code for backward compatibility.
     */
    
    public static Document createXml(String rootName) {
        return createXML(rootName);
    }
    
    /**
     * Creates a new XML document with the root element given the tag name
     * specified in the parameter.
     */
    
    public static Document createXML(String rootName) {
        Document doc = new DocumentImpl();
        doc.appendChild(doc.createElement(rootName));
        return doc;
    }
    
    /**
     * Parses an XML document from a URL specified as a string.
     */
	
    public static Document parseXMLFromURLString(String url) throws XMLHelperException {
        return parseXMLFromURL(convertStringToURL(url));
    }
    
    /**
     * Parses an XML document from a file specified by a file path string.
     */
    
    public static Document parseXMLFromFile(String fileName) throws XMLHelperException {
        return parseXMLFromFile(new File(fileName));
    }
    
    /**
     * Parses an XML document from a file specified as a file object.
     */
    
    public static Document parseXMLFromFile(File f) throws XMLHelperException {
        try {
            /*
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(f);
            */
            FileReader fr = new FileReader(f);
            InputSource is = new InputSource(fr);
            return parseXMLFromInputSource(is);
        } catch (IOException ex) {
            throw new XMLHelperException("Unable to read from source string", ex);
        } /*catch (ParserConfigurationException ex) {
            throw new XMLHelperException("Unable to configure the parser", ex);  
        } catch (SAXParseException ex) {
            int lineNumber = ex.getLineNumber();
            throw new XMLHelperException("Unable to parse the input (line " + lineNumber + ")", ex);
        } catch (SAXException ex) {
            throw new XMLHelperException("Unable to parse the input", ex);
        }*/
    }
    
    /**
     * Parses an XML document from a URL specified as a URL object
     */
	
    public static Document parseXMLFromURL(URL url) throws XMLHelperException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            URLConnection inConnection = url.openConnection();
            return builder.parse(inConnection.getInputStream());
        } catch (IOException ex) {
            throw new XMLHelperException("Unable to read from source string", ex);
        } catch (ParserConfigurationException ex) {
            throw new XMLHelperException("Unable to configure the parser", ex);  
        } catch (SAXParseException ex) {
            int lineNumber = ex.getLineNumber();
            throw new XMLHelperException("Unable to parse the input (line " + lineNumber + ")", ex);
        } catch (SAXException ex) {
            throw new XMLHelperException("Unable to parse the input", ex);
        }
    }
    
    /**
     * Parses an XML document that is stored in a string
     */
	
    public static Document parseXMLFromString(String source) throws XMLHelperException {	
        InputSource is = new InputSource(new StringReader(source));
        return parseXMLFromInputSource(is);
    }
    
    private static Document parseXMLFromInputSource(InputSource is) throws XMLHelperException {
        Document doc = null;
        
        try {
            DOMParser parser = new DOMParser();
            parser.parse(is);
            doc = parser.getDocument();
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to read from source string", ioe);
        } catch (SAXParseException ex) {
            int lineNumber = ex.getLineNumber();
            throw new XMLHelperException("Unable to parse the input (line " + lineNumber + ")", ex);
        } catch (SAXException saxe) {
            throw new XMLHelperException("Unable to parse the given string", saxe);  
        }
        return doc;	
    }
            
    /**
     * Performs an XSL transformation on an XML document and returns the resulting XML document.
     * @param xmlDoc The data XML document that is being transformed.
     * @param xslDoc The XSL transformation document providing the transformation rules.
     * @result An XML document that represents the transformation.
     */
    
    public static Document transformXML(Document xmlDoc, Document xslDoc) throws XMLHelperException {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new DOMSource(xslDoc));
            DOMResult result = new DOMResult();
            transformer.transform(new DOMSource(xmlDoc), result);
            org.w3c.dom.Node resultNode = result.getNode();
            if (resultNode instanceof Document) return (Document)resultNode;
            else return result.getNode().getOwnerDocument();
        } catch (TransformerConfigurationException ex) {
            throw new XMLHelperException("Unable to perform transform " + 
                                         ex.getLocationAsString(), ex);
        } catch (TransformerException ex) {
            throw new XMLHelperException("Unable to perform transform " +
                                         ex.getLocationAsString(), ex);
        }
    }
    
    /**
     * Writes an XML document formatted in a fairly standard way to a specified PrintStream.
     * This method is useful for dumping XML to standard out or standard error.
     */
    
    public static void outputXML(Document doc, PrintStream stream) throws XMLHelperException {
        try {
            OutputFormat of = new OutputFormat(doc);
            of.setIndenting(true);
            XMLSerializer serializer = new XMLSerializer(stream, of);
            serializer.serialize(doc);
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to write to the given print stream", ioe);
        }
    }
    
    /**
     * Outputs a character based file to the path specified in the fileName parameter.
     * The text to write to that file is given in the text parameter.
     */
	
    public static void outputTextToFile(String text, String fileName) throws XMLHelperException {
        File file = new File(fileName);
        outputTextToFile(text, file);
    }
    
    /**
     * Outputs a character based file to the path specified in the fileName parameter.
     * The text to write to that file is given in the text parameter.
     */
	
    public static void outputTextToFile(String text, File file) throws XMLHelperException {
        try {
            File dir = new File(file.getParent());
            dir.mkdirs();
            FileWriter fw = new FileWriter(file);
            fw.write(text);
            fw.flush();
            fw.close();
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to write to the given file", ioe);
        }
    }
    
    /**
     * Output an XML file, formatted in a fairly standard way, to a File.
     */
    
    public static void outputXMLToFile(Document doc, File file) throws XMLHelperException {
        try {
            if (doc == null) throw new IOException("Output XML document was null");
            if (file == null) throw new IOException("Bad File given");
            OutputFormat of = new OutputFormat(doc, "UTF-8", true);
            FileOutputStream fos = new FileOutputStream(file);
            XMLSerializer serializer = new XMLSerializer(fos, of);
            serializer.serialize(doc);
            fos.close();
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to write to the given file", ioe);
        }
    }
    
    /**
     * Output an XML file, formatted in a fairly standard way, to a File.
     */
    
    public static void outputXMLToFile(Document doc, String fileName) throws XMLHelperException {
        outputXMLToFile(doc, new File(fileName));
    }
	
    public static String convertXMLToString(Document doc) throws XMLHelperException {
        try {
            OutputFormat of = new OutputFormat(doc);
            of.setIndenting(true);
            StringWriter sw = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(sw, of);
            serializer.serialize(doc);
            return sw.toString();
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to write to the string", ioe);
        }
    }
	
    /**
     * Copies the children of the mergeFromXML element and places them as children
     * in the mergeToXML. Of course it recursively gets the children as well. If childrenOnly
     * is set to false, just insert the mergeFromXML under the mergeToXML.
     */
	
    public static void mergeXML(Element mergeToXML, Element mergeFromXML, boolean childrenOnly) {
        Document toDoc = mergeToXML.getOwnerDocument();
        Element copyElem = (Element)(toDoc.importNode(mergeFromXML,true));
        if (childrenOnly) {
            NodeList nlist = copyElem.getChildNodes();
            for (int i=0; i < nlist.getLength(); i++) {
                org.w3c.dom.Node n = nlist.item(i);
                mergeToXML.appendChild(n);
            }
            return;
        } else {
            mergeToXML.appendChild(copyElem);
        }
    }
    
    /**
     * Retrieves an HTML document from the web and tidies it up to be a well formed
     * XML (XHTML) document.
     */
	/*
    public static Document tidyHTML(String url) throws XMLHelperException {
        return tidyHTML(convertStringToURL(url));
    }
    */
     
    /**
     * Retrieves an HTML document from the web and tidies it up to be a well formed
     * XML (XHTML) document.
     */
    /*
    public static Document tidyHTML(URL url) throws XMLHelperException {
        try {
            URLConnection inConnection = url.openConnection();
            if (inConnection.getContentType().startsWith("text/xml") ||
                inConnection.getContentType().startsWith("text/xhtml")) {
                // All ready an XML source
                return parseXMLFromURL(url);
            } else if (inConnection.getContentType().startsWith("text/html")) {
                // An HTML source
                InputStream is = inConnection.getInputStream();
				
                // Clean the input stream
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                            
                int totalBytes = 0;
                byte[] buffer = new byte[16384];
				
                while (true) {
                    int bytesRead = is.read(buffer, 0, buffer.length);
                    if (bytesRead < 0) break;
                    // Remove binary bellow space except tab and newline
                    for (int i=0; i < bytesRead; i++) {
                        byte b = buffer[i];
                        if (b < 32 && b!= 10 && b != 13 && b != 9) b = 32;
                        buffer[i] = b;
                    }
                    out.write(buffer, 0, bytesRead);
                    totalBytes += bytesRead;
                }
                is.close();
                out.close();
				
                String outContent = out.toString();
                InputStream in = new ByteArrayInputStream(out.toByteArray());
				
                org.w3c.tidy.TagTable tags = org.w3c.tidy.TagTable.getDefaultTagTable();
                tags.defineBlockTag("script");
                tags.defineBlockTag("nowrap");
                
                Tidy tidy = new Tidy();
				
                //tidy.setMakeClean(true);
                tidy.setShowWarnings(false);
                tidy.setXmlOut(true);
                tidy.setXmlPi(false);
                tidy.setDocType("omit");
                //tidy.setQuoteNbsp(true);
                //tidy.setQuoteAmpersand(true);
                tidy.setXHTML(false);
                tidy.setRawOut(true);
                tidy.setNumEntities(true);
                tidy.setQuiet(true);
                tidy.setFixComments(true);
                tidy.setIndentContent(true);
                tidy.setCharEncoding(org.w3c.tidy.Configuration.ASCII);
				
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                //Document resultDoc = tidy.parseDOM(in, null);
                //if (result == null) System.err.println("Null sucker");
                //tidy.pprint(resultDoc, baos);
				
                org.w3c.tidy.Node tNode = tidy.parse(in, baos);
                String result = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" +
                                baos.toString();
                // Strip the DOCTYPE and script elements
                int startIndex = 0;
                int endIndex = 0;
                if ((startIndex = result.indexOf("<!DOCTYPE")) >= 0) {
                    endIndex = result.indexOf(">",startIndex);
                    result = result.substring(0,startIndex) + 
                             result.substring(endIndex + 1, result.length());
                }
                while ((startIndex = result.indexOf("<script")) >= 0) {
                    endIndex = result.indexOf("</script>");
                    result = result.substring(0,startIndex) +
                             result.substring(endIndex + 9, result.length());
                }
                
                in.close();
                baos.close();
				
                return parseXMLFromString(result);
                
            } else {
                throw new XMLHelperException("Unable to tidy content type: " +
                                             inConnection.getContentType());
            }
        } catch (IOException ioe) {
            throw new XMLHelperException("Unable to perform input/output", ioe);
        }
    }
	*/
	
    private static URL convertStringToURL(String url) throws XMLHelperException {
        try {
            return new URL(url);
        } catch (MalformedURLException murle) {
            throw new XMLHelperException(url + " is not a well formed URL", murle);
        }
    }
    
    public static void main(String args[]) {
        if (args.length < 3) {
            printUsage();
            System.exit(0);
        }
        File inXMLFile = new File(args[0]);
        File xslFile = new File(args[1]);
        File outXMLFile = new File(args[2]);
        
        try {
            Document inXMLDoc = XMLHelper.parseXMLFromFile(inXMLFile);
            Document xslDoc = XMLHelper.parseXMLFromFile(xslFile);
            Document outXMLDoc = XMLHelper.transformXML(inXMLDoc, xslDoc);
            XMLHelper.outputXMLToFile(outXMLDoc, outXMLFile);
            System.out.println("Wrote resulting XML to " + outXMLFile.getAbsolutePath());
        } catch (XMLHelperException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    private static void printUsage() {
        System.out.println("XMLHelper usage:\n");
        System.out.println("~> XMLHelper data_file.xml transform.xsl result.xml"); 
    }
}
