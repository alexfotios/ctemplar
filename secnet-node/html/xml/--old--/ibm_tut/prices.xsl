<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xslt">
    
    <xsl:output method="html"/>
    
    <xsl:template match="/">
		<html>
			<head><title>Products</title></head>
			<body>
				<xsl:variable name="priceSum">
					<xsl:call-template name="priceSum">
						<xsl:with-param name="productList" select="/*/Product"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="priceSumDivideAndConquer">
					<xsl:call-template name="priceSumDivideAndConquer">
						<xsl:with-param name="productList" select="/*/Product"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="priceSumSegmented">
					<xsl:call-template name="priceSumSegmented">
						<xsl:with-param name="productList" select="/*/Product"/>
						<xsl:with-param name="segmentLength" select="5"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="priceSumCombination">
					<xsl:call-template name="priceSumCombination">
						<xsl:with-param name="productList" select="/*/Product"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="priceSumTail">
					<xsl:call-template name="priceSumTail">
						<xsl:with-param name="productList" select="/*/Product"/>
					</xsl:call-template>
				</xsl:variable>
				
				<table border="1">
					<tr>
						<td><b>Name</b></td>
						<td><b>Price</b></td>
					</tr>
					<xsl:for-each select="/Products/Product">
						<tr>
							<td><xsl:value-of select="Name"/></td>
							<td><xsl:value-of select="Price"/></td>
						</tr>
					</xsl:for-each>
					<tr>
						<td align="right"><b>Simple - Total Price:</b></td>
						<td><b><xsl:value-of select="concat('$',$priceSum)"/></b></td>
					</tr>
					<tr>
						<td align="right"><b>D &amp; C - Total Price:</b></td>
						<td><b><xsl:value-of select="concat('$',$priceSumDivideAndConquer)"/></b></td>
					</tr>
					<tr>
						<td align="right"><b>Segment - Total Price:</b></td>
						<td><b><xsl:value-of select="concat('$',$priceSumSegmented)"/></b></td>
					</tr>
					<tr>
						<td align="right"><b>Combo - Total Price:</b></td>
						<td><b><xsl:value-of select="concat('$',$priceSumCombination)"/></b></td>
					</tr>
					<tr>
						<td align="right"><b>Tail - Total Price:</b></td>
						<td><b><xsl:value-of select="concat('$',(round($priceSumTail * 100)) div 100)"/></b></td>
					</tr>
				</table>
			</body>
		</html>
    </xsl:template>
    
    <xsl:template name="priceSum">
    	<xsl:param name="productList"/>
    	<xsl:choose>
    		<xsl:when test="$productList">
    			<xsl:variable name="recursive_result">
    				<xsl:call-template name="priceSum">
    					<xsl:with-param name="productList" select="$productList[position() &gt; 1]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:value-of select="number(substring-after($productList[1]/Price,'$')) + $recursive_result"/>
    		</xsl:when>
    		<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="priceSumDivideAndConquer">
    	<xsl:param name="productList"/>
       	<xsl:choose>
       		<xsl:when test="count($productList) = 1">
       			<xsl:value-of select="number(substring-after($productList[1]/Price,'$'))"/>
       		</xsl:when>
    		<xsl:when test="$productList">
    			<xsl:variable name="halfIndex" select="floor(count($productList) div 2)"/>
    			<xsl:variable name="recursive_result1">
    				<xsl:call-template name="priceSumDivideAndConquer">
    					<xsl:with-param name="productList" select="$productList[position() &lt;= $halfIndex]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="recursive_result2">
    				<xsl:call-template name="priceSumDivideAndConquer">
    					<xsl:with-param name="productList" select="$productList[position() &gt; $halfIndex]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:value-of select="$recursive_result1 + $recursive_result2"/>
    		</xsl:when>
    		<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="priceSumSegmented">
    	<xsl:param name="productList"/>
    	<xsl:param name="segmentLength" select="5"/>
    	<xsl:choose>
    		<xsl:when test="count($productList) &gt; 0">
    			<xsl:variable name="recursive_result1">
    				<xsl:call-template name="priceSum">
    					<xsl:with-param name="productList" select="$productList[position() &lt;= $segmentLength]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="recursive_result2">
    				<xsl:call-template name="priceSumSegmented">
    					<xsl:with-param name="productList" select="$productList[position() &gt; $segmentLength]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:value-of select="$recursive_result1 + $recursive_result2"/>
    		</xsl:when>
    		<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
    	</xsl:choose>	
    </xsl:template>
    
    <xsl:template name="priceSumCombination">
    	<xsl:param name="productList"/>
    	<xsl:param name="threshold" select="5"/>
    	<xsl:choose>
    		<xsl:when test="count($productList) &gt; $threshold">
    			<xsl:variable name="halfIndex" select="floor(count($productList) div 2)"/>
    			<xsl:variable name="recursive_result1">
    				<xsl:call-template name="priceSumCombination">
    					<xsl:with-param name="productList" select="$productList[position() &lt;= $halfIndex]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="recursive_result2">
    				<xsl:call-template name="priceSumCombination">
    					<xsl:with-param name="productList" select="$productList[position() &gt; $halfIndex]"/>
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:value-of select="$recursive_result1 + $recursive_result2"/>
    		</xsl:when>
    		<xsl:otherwise>
    			<xsl:call-template name="priceSum">
    				<xsl:with-param name="productList" select="$productList"/>
    			</xsl:call-template>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template name="priceSumTail">
    	<xsl:param name="productList"/>
    	<xsl:param name="result" select="0"/>
    	<xsl:choose>
    		<xsl:when test="$productList">
    			<xsl:call-template name="priceSumTail">
    				<xsl:with-param name="productList" select="$productList[position() &gt; 1]"/>
    				<xsl:with-param name="result" select="$result + number(substring-after($productList[1]/Price,'$'))"/>
    			</xsl:call-template>
    		</xsl:when>
			<xsl:otherwise><xsl:value-of select="$result"/></xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
