<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xslt">
    
    <xsl:output method="html"/>
    
    <xsl:template match="/">
    	<xsl:variable name="rows" select="number(/Test/MultiplicationTable/@Rows)"/>
    	<xsl:variable name="cols" select="number(/Test/MultiplicationTable/@Columns)"/>
    	
    	<html>
    	<head><title>Multiplication table <xsl:value-of select="concat($rows,concat(' x ', $cols))"/></title></head>
    	<body>
    	<table cellpadding="2" cellspacing="1" bgcolor="black">
    		<xsl:call-template name="drawRow">
    			<xsl:with-param name="currentRow" select="0"/>
    			<xsl:with-param name="totalRows" select="$rows"/>
    			<xsl:with-param name="totalCols" select="$cols"/>
    		</xsl:call-template>
    	</table>
    	</body></html>
    </xsl:template>
    
    <xsl:template name="drawRow">
    	<xsl:param name="currentRow"/>
    	<xsl:param name="totalRows"/>
    	<xsl:param name="totalCols"/>
	    <xsl:if test="$currentRow &lt;= $totalRows">
	    	<tr>
		    	<xsl:call-template name="drawCell">
		    		<xsl:with-param name="currentRow" select="$currentRow"/>
		    		<xsl:with-param name="currentCol" select="0"/>
		    		<xsl:with-param name="totalCols" select="$totalCols"/>
		    	</xsl:call-template>
	    	</tr>
	    	<xsl:call-template name="drawRow">
	    		<xsl:with-param name="currentRow" select="$currentRow  + 1"/>
				<xsl:with-param name="totalRows" select="$totalRows"/>
	 			<xsl:with-param name="totalCols" select="$totalCols"/>
	  		</xsl:call-template>
	    </xsl:if>
    </xsl:template>
    
    <xsl:template name="drawCell">
    	<xsl:param name="currentRow"/>
    	<xsl:param name="currentCol"/>
    	<xsl:param name="totalCols"/>
    	<xsl:if test="$currentCol &lt;= $totalCols">
			<xsl:variable name="bgColor">
				<xsl:choose>
					<xsl:when test="$currentRow = 0 or $currentCol = 0">#CCCCCC</xsl:when>
					<xsl:otherwise>white</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="value">
				<xsl:choose>
					<xsl:when test="$currentRow = 0 and $currentCol = 0"></xsl:when>
					<xsl:when test="$currentRow = 0"><xsl:value-of select="$currentCol"/></xsl:when>
					<xsl:when test="$currentCol = 0"><xsl:value-of select="$currentRow"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$currentRow * $currentCol"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<td bgcolor="{$bgColor}" align="center" valign="top">
				<xsl:value-of select="$value"/>
			</td>
    		<xsl:call-template name="drawCell">
    			<xsl:with-param name="currentRow" select="$currentRow"/>
    			<xsl:with-param name="currentCol" select="$currentCol + 1"/>
    			<xsl:with-param name="totalCols" select="$totalCols"/>
    		</xsl:call-template>
    	</xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
