The files in this directory contain the code associated
with the developerWorks XSL recursion article written by
Jared Jackson - August, 2002.

The files are written in either Java, XML, or XSL, and
together implement all of the concepts and examples
contained in the article.

Provided also are the XML and XSL libraries distributed
by Apache (xerces and xalan). Their use is protected by
the Apache License Agreement.

The code groups into three examples:

1. Multiplication Table

The multiplication table shows how iteration on a number
is done in XSL. The code produces an HTML page from a
simple XML document. To do the transformation enter:

java -classpath .;xerces.jar;xalan.jar XMLHelper mult_table.xml mult_table.xsl mult_table.html

or Windows users can run: mult_table.bat

2. Products and Prices

This example shows all of the various methods described
in the article for implementing recursion in XSL. The
input is an XML file with a list of products and prices.
The output is an HTML page listing those products, their
prices, and the total price computed using the various
techniques. To do the transformation enter:

java -classpath .;xerces.jar;xalan.jar XMLHelper products.xml prices.xsl product_prices.html

or windows users can run: product_prices.bat

3. Tower of Hanoi

This is a two stage transform that converts an XML file
into another XML file representing the solution to the
Tower of Hanoi game. The resulting XML solution file can
then be transformed into an SVG file (that's a graphics
file in XML that requires a browser plug-in from Adobe to
view). To do the transform enter:

java -classpath .;xerces.jar;xalan.jar XMLHelper hanoi.xml hanoi.xsl hanoi_solution.xml

then

java -classpath .;xerces.jar;xalan.jar XMLHelper hanoi_solution.xml hanoi_svg.xsl hanoi.svg

or windows users can run: hanoi.bat