<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="/">
    <xsl:call-template name="processNode"/>
</xsl:template>

	
<xsl:template name="processNode">

    <xsl:for-each select="node">      

    	<div class="elementDiv" id="{concat('element-', generate-id(/))}">						        
			
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			
                <tr>
                    <td width="100%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        					<tr>
                				<td nowrap="true" id="{concat('flag-', generate-id(/))}" class="flagCell" onclick="fold('element1')"></td>								
								<td> </td>                    
                            	<td id="{concat('title-', generate-id(/))}" class="titleCell" nowrap="true">
									<xsl:value-of select="@name"/>
								</td>
                            	<td> </td>
                            	<td id="{concat('description-', generate-id(/))}" class="descriptionCell">
									<xsl:value-of select="@description"/>
								</td>
                            	<td width="100%"> </td>
        					</tr>                            
                        </table>
                    </td>
                </tr>							
                        
                <tr>
                    <td class="contentCell" id="{concat('content-', generate-id(/))}" width="100%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td> </td>
							</tr>
                            <tr>
                                <td></td>                    
                 				<td width="100%">								
								
                		            <!-- content start -->				
            															
                        			<xsl:if test="node">
                        				<xsl:call-template name="processNode"/>
                        			</xsl:if>            						
									
            		            	<!-- content end -->

        	            		</td>
                    		</tr>
                            <tr>
								<td> </td>
							</tr>
                   		</table>
                  	</td>					
              	</tr>
          	</table>
	   	</div>						

    </xsl:for-each>
	
</xsl:template>
	

</xsl:stylesheet>



 