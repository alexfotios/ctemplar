<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="/">	
	<div id="menuDiv" style="width:250px;display:block;overflow-x:auto;">
	    <xsl:call-template name="processNode"/>
	</div>
</xsl:template>


	
<xsl:template name="processNode">

	<xsl:for-each select="node">      		
			<!--
    		<xsl:if test="node">
			-->			
    			<div class="elementDiv" id="{concat('element',string(number(@id)-1))}">
					<xsl:if test="@style"> <!-- only a temporary bug fix -->
						<xsl:attribute name="style">
							<xsl:value-of select="@style"/>
						</xsl:attribute>
					</xsl:if>				
    		<!--
			</xsl:if>																				        
			-->
			
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			
                <tr>
                    <td width="100%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        					<tr>
							
								<xsl:choose>
									<xsl:when test="node">
                						<td nowrap="" id="{concat('flag',string(@id))}" class="flagCell" onclick="fold('{concat('element',string(@id))}')">+</td>										 
									</xsl:when>
									<xsl:otherwise>
										<td><input type="radio" name="insaw" value="{concat(string(../@name),'.',string(@name))}"/></td>
									</xsl:otherwise>										
								</xsl:choose>	
									
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>													                   
                            	<td id="{concat('title',string(@id))}" class="titleCell" nowrap="">
									<xsl:value-of select="@name"/>
								</td>
                            	<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                            	<td id="{concat('description',string(@id))}" class="descriptionCell">
									<!-- <xsl:value-of select="@description"/> -->
									<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								</td>
                            	<td width="100%"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
        					</tr>                            
                        </table>
                    </td>
                </tr>							
                        
                <tr>
                    <td class="contentCell" id="{concat('content',string(@id))}" width="100%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>
                            <tr>
            					<td class="spacerCell">
            						<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
            					</td>	                  
                 				<td width="100%">							
								
                		            <!-- content start -->				
									<xsl:choose>							
                            			<xsl:when test="node">
                            				<xsl:call-template name="processNode"/>
                            			</xsl:when>
										<xsl:otherwise>
    										<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
										</xsl:otherwise>           						
									</xsl:choose>
            		            	<!-- content end -->

        	            		</td>
                    		</tr>
                            <tr>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>
                   		</table>
                  	</td>					
              	</tr>
          	</table>
			
	   	</div>						

    </xsl:for-each>
	
	
</xsl:template>
	

</xsl:stylesheet>



 