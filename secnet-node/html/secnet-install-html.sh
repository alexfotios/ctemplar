# create sys/html dir structure
mkdir $SECNET_PATH/sys/html
chmod 755 $SECNET_PATH/sys/html
#mkdir $SECNET_PATH/sys/html/snippets
#mkdir $SECNET_PATH/sys/html/xml

# copy snippets and xml/xsl files from src/html to sys/html
cp -R $SECNET_PATH/src/html/* $SECNET_PATH/sys/html

# set permissions
chmod -R 755 $SECNET_PATH/sys/html/*

# copy a proper index.html file to apache's htdocs dir
echo "<h2>Trying to be smart?</h2>" > $SECNET_PATH/sys/apache/htdocs/index.html
chmod 644 $SECNET_PATH/sys/apache/htdocs/index.html
