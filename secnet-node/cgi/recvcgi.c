#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#include <stdarg.h>

/***********/

#include <libxml/xmlmemory.h>
#include <libxml/debugXML.h>
#include <libxml/HTMLtree.h>
#include <libxml/xmlIO.h>
#include <libxml/DOCBparser.h>
#include <libxml/xinclude.h>
#include <libxml/catalog.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

/***********/

#define IMAP_PORT 143
#define SMTP_PORT 25

#define STRING_SIZE 1024

#define PATH_LENGTH 1024
#define SNIPPET_LENGTH 65535
#define EVAL_SNIPPET_LENGTH 265535
#define IMAP_MSG_SIZE 65535
#define TREE_SIZE 200000
#define HTML_SIZE 400000


char *name[STRING_SIZE], *value[STRING_SIZE], *This_Session[STRING_SIZE], *Headers[STRING_SIZE], *InBuf, *S, *sToken;
int HeaderNum = 0; 

char *msgHeaders, *msgBody;

char *secnet_path;

char* TLP_Value; 

/**********/

//will free the memory allocated to a NULL teminated array of strings
void freeStringArray(char** sa)
{
    int i = 0;
    char* temp;
    
    while (temp = *(sa+i++)) free(temp);
    
    free(sa);
}


//finds length of null terminated array of strings
int salen (char** sa)
{
    int i = 0;
    char** p = sa;
    while(*p++) i++;
    return i;
}

// splits on substring
char** split2(char* s, char* delimiter) //supports up to a max of 512 tokens
{
    //printf("\n\n#################################### SPLIT #########################################\n\n");
    
    char **tokens = (char**) malloc(512);
    
    //char *t, *tend;
    char *p1 = s, *p2;
    
    //printf("the received string is: %s", s);
    
    int i = 0;
    
    while (p2 = strstr(p1, delimiter))
    {
        //printf("found split token");
        tokens[i] = (char*) malloc(p2-p1+1);        
        tokens[i+1] = NULL; 

        memcpy(tokens[i], p1, p2-p1);                
        *(tokens[i++] + (p2-p1)) = NULL;
        
        p1 = p2 + strlen(delimiter);        
    }    

    tokens[i] = (char*) malloc(strlen(p1) + 1);
    tokens[i+1] = NULL;        
    
    strcpy(tokens[i], p1);    
    
    /*
    for (i = 0; i < salen(tokens); i++)
    {
        printf("\n\n***************************\n%s\n*************************\n\n", tokens[i]);
    }
    
    printf("\n\n################################## END SPLIT ###########################################\n\n");
    */
    
    return tokens;    
}

//splits on a number of delimiting chars (supplied as a string of chars)
char** split3(char* s, char* d)
{
    char **tokens = (char**) malloc(512); tokens[0] = NULL;
    int tokenSize = 1024;
       
    char* t = strtok(s, d);
    
    int i = 0;
    
    while (t)
    {        
        tokens[i] = (char*) malloc(tokenSize);
        tokens[i+1] = NULL;      
        strcpy(tokens[i++], t);               
        t = strtok(NULL, d);
    }
    
    return tokens;
}

char** split(char* s, char* delimiter)
{
    //printf("got in");
    //exit(0);
    
    char **tokens = NULL;
    //char *t, *tend;
    char *p1 = s, *p2;
    
    int i = 0;
    
    while (p2 = strstr(p1, delimiter))
    {
        //printf("%d<br>----<br>", i+1);
        tokens = realloc(tokens, i+2);        
        *(tokens+i) = (char*) malloc(p2-p1+1);        
        *(tokens+i+1) = 0; 
        //printf("%d",p2-p1); exit(0);
        
        //t = (char*) malloc(p2 - p1);
        memcpy(*(tokens+i), p1, p2-p1);                
        *(*(tokens+i++) + (p2-p1)) = 0;
        
        //printf("tokens[%d]=%s<br>----<br>", i-1, *(tokens+i-1));
        
        //strcpy(*(tokens+i++), t);
        p1 = p2 + strlen(delimiter);
        
        //printf("%s<br>----<br>", strstr(p1, delimiter));
    }
    
    //tokens = realloc(tokens, i+1); //why does this line break it?
    // *(tokens+i) = (char*) malloc(strlen(p1) + 1);
    // *(tokens+i+1) = 0;        
    
    strcpy(*(tokens+i), p1);
    //memcpy(*(tokens+i), p1, strlen(p1));                
    // *(*(tokens+i) + strlen(p1)) = 0;    
    
    //printf("tokens[%d]=%s<br>----<br>", i, *(tokens+i));
    //exit(0);

    //int j;
    for (i = 0; i < 4; i++)
    {
        printf("%s<br>----<br>", *(tokens+i));
    }
    //exit(0);
    
    
    return tokens;    
}


char* evaluateSnippet(char* html, char** sa)
{
    //printf("\n\################################# EVALUATE SNIPPET ############################################\n\n");
    
    char* placeholder = "%s";
    
    char **tokens = NULL;
    
    int i = 0; 
    
    //split string over placeholder string
    
    /*
    printf("\n\n****** evaluateSnippet received this html tail ************************************\n\n%s\n\n*************\n\n", html + 9 * strlen(html) / 10);
    */
    
    tokens = split2(html, placeholder);
    
    
    /*
    int j;
    printf("returned tokens:\n<br><br>");
    for (j = 0; j < 4; j++)
    {
        printf("%s<br>----<br>", tokens[j]);
    }
    exit(0);    
    */    
    
    int len = salen(tokens);
    
    //printf("len=%d\n<br>--<br>", len);
    //exit(0);
    
    char *newhtml = (char*) malloc(EVAL_SNIPPET_LENGTH); *newhtml = NULL;
    
    //sandwich values with tokens    
    for (i = 0; i < len - 1; i++)
    {
        strcat(newhtml, tokens[i]);
        strcat(newhtml, sa[i]);
    }    
    
    strcat(newhtml, tokens[i]);
    
    /*
    printf("\n\n****** evaluateSnippet produced this html tail with %d tokens ************************************\n\n%s\n\n*************\n\n", len, newhtml + 9 * strlen(newhtml) / 10);
    
    printf("\n\n##################################### END EVALUATE SNIPPET ########################################\n\n");
    */
    
    return newhtml;
    
}


/* printSnippet() is implemented as C-Style Variadic function */
/* It only processes arguments of type pointer to char */
void printSnippet(const char* arg, ...)
{
    //printf("\n\n#################################### PRINT SNIPPET %s #########################################\n\n", arg);
    va_list alist;
    char*  s;
    int i;
    
    FILE* snippet;
    char *spath, *html, *line;
    
    //char **sa = NULL;
    char** sa = (char**) malloc(32);
    sa[0] = NULL;
    
    char *p = (char*) malloc(1024); *p = 0;
    strcat(p, secnet_path); strcat(p, "/sys/html/snippets/");

    //printf("<h3>Snippet path is: %s</h3>", p); exit(0);

    va_start(alist, arg);

    for (s = arg,i = 0; s != NULL; s = va_arg(alist, const char *), i++)
    {
        //printf("%s", s); exit(0);
        if (i == 0) //first arg is snippet file name, so get the html it contains
        {	    
            spath = (char*) malloc(PATH_LENGTH); *spath=0;
            html = (char*) malloc(SNIPPET_LENGTH); *html=0;
            line = (char*) malloc(SNIPPET_LENGTH); *line=0;
            
            strcat(spath, p); strcat(spath, s); strcat(spath, ".htm");
            
	    //printf("<h3>Current snippet path is: %s</h3>", spath); exit(0);                        
            
            if (strstr(spath, "InboxElement")){
                //printf(spath); exit(1);
            }
            
            snippet = fopen(spath, "r");            
            
            //printf("got to here"); exit(1);
            
            //if (strstr(spath, "InboxElement")){
                //printf(snippet); exit(1);
            //}
            
            while (!feof(snippet))
            {
                fgets(line, SNIPPET_LENGTH, snippet);
                strcat(html, line);
                *line = 0;
            }

	    //printf("after true read"); exit(0);
            
            fclose(snippet);
            
            if (strstr(spath, "MiddlePanelEnd")){
                //printf("========================\n%s\n========================", html); exit(1);
            }
        }
        else
        {
            //accumulate arg in arg array (array of strings)
            //sa = realloc(sa, i);
            
            sa[i-1] = (char*) malloc(strlen(s) + 1);
            sa[i] = NULL;
        
            strcpy(sa[i-1], s);
        }
    }

    va_end(alist);

    /*
    int j;
    for (j = 0; j < i-1; j++)
        printf("%s<br>", sa[j]);
    */
    
    
    //printf("\n\n#################################### EVALUATION START %s #########################################\n\n", arg);
    
    //if (i > 1){
    	char *snipeval = evaluateSnippet(html, sa);
    	printf(snipeval);
    	free(snipeval);
    //}
    //else{
        //printf("no evaluation");
    //	printf(html);
    //}

    free(html);
    free(line);
    free(spath);
    
    //printf("\n\n#################################### EVALUATION END %s #########################################\n\n", arg);    
    //printf("\n\n#################################### END PRINT SNIPPET %s #########################################\n\n", arg);
}


void cleanHeaders()
{
  int i;

  for(i = 0; i < HeaderNum; i++)
    Headers[i] += 55;
}


void cleanBody()
{
  int i;

  S += 29;
  *(S + strlen(S) - 25) = '\0';
}   


void makeSubstrings(const char *DelimitString)
{
  static int i = -1;

  char *old;

  //printf(" ");
  HeaderNum = ++i;

  if ( S = strstr(S, DelimitString) )
  {

    if (i > 0) *(S-3) = '\0';
    old = S += 2;
    makeSubstrings(DelimitString);
  }
  
  if (S == NULL) S = old;
  Headers[i--] = old;
}


void makeSubstrings2(const char *sDelimiter)
{    
	static int i = 0;

        i++;

	if (sToken){
		//printf("<pre>%s -- %d</pre><br>\n", sToken, strlen(sToken));

		if (*sToken == '*' && strstr(sToken, "* OK") != sToken)
		{
			HeaderNum++;
			Headers[HeaderNum-1] = (char*)malloc(2048);
			//printf("<pre>even sToken: %s</pre><br>\n", sToken);
		}
		else if (strstr(sToken, "Subject") == sToken ||
			     strstr(sToken, "From") == sToken ||
				 strstr(sToken, "Date") == sToken ||
                                      strstr(sToken, "X-SecNetIE-TLP") == sToken)
		{
                        strcat(Headers[HeaderNum-1], sToken);
		}

		sToken = strtok(NULL, sDelimiter);
		makeSubstrings2(sDelimiter);
	}
	else
		return;

}


char *IMAP_operation()
{
  //diag();
  //exit(1);
    
    
  char   *Token, *Helper, *buf;
  int    Socket, i, j, MsgNum;
  struct sockaddr_in SockAddr;
  struct hostent *IMAP_Host;

  char *Buffer  = (char *) malloc(IMAP_MSG_SIZE); *Buffer = 0;

  /*Setup OutSocket*/
  if ( (Socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 )              /* make socket */
  { 
      printf("socket()");
      exit(0);
    //fprintf( dlog, perror("socket(): ") );
    //fflush(dlog);
  }

  if ( (IMAP_Host = gethostbyname(This_Session[2])) == 0 )           /* get host */
  {
      printf("gethostbyname()");
      exit(0);    
      //fprintf( dlog, perror("gethostbyname()") );
    //fflush(dlog);
  }

  
  /*Setup Socket*/
  memset(&SockAddr, 0, sizeof(struct sockaddr_in));
  memcpy(&SockAddr.sin_addr, IMAP_Host->h_addr, IMAP_Host->h_length);   /* set host */
  SockAddr.sin_port = htons(IMAP_PORT);                                 /* set port */
  SockAddr.sin_family = AF_INET;

  //printf("got to here");exit(1);
    
  /*bind is done automatically by connect*/
  if ( connect( Socket, (struct sockaddr *)&SockAddr, sizeof(SockAddr) ) < 0 )
  {
      printf("connect()");
      exit(0);         
        //fprintf( dlog, perror("connect()") );
        //fflush(dlog);
  }
  

  recv(Socket, Buffer, IMAP_MSG_SIZE, 0);
  

  //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);        /* print message */
  //fflush(dlog);

  

  if ( !strcmp(This_Session[0], "headers") )
  {
    strcpy(Buffer, "1 LOGIN ");  /* load buffer */
    strcat(Buffer, This_Session[3]); /* append username */
    strcat(Buffer, " ");
    strcat(Buffer, This_Session[4]); /* append password */ 
    strcat(Buffer, "\n");

    write(Socket, Buffer, strlen(Buffer)); /* write data */
    Buffer[strlen(Buffer)] = '\0';

    //fprintf(dlog, "Step 1 IMAP Message To Be Sent = %s\n", Buffer);               /* print message */
    //fflush(dlog);

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0); /*receive login response */        

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);     /* print message */
    //fflush(dlog);

    //if login fails just return with latest response in the buffer
    if (strstr(Buffer, "1 NO Authentication failed."))
    {
	   close(Socket);
       return Buffer;
    }
  
    strcpy(Buffer, "2 SELECT ");            /* load buffer */
    strcat(Buffer, This_Session[3]);        /* append username */

    strcat(Buffer, "\n");
    write(Socket, Buffer, strlen(Buffer));  /* write data */

    Buffer[strlen(Buffer)] = '\0';
    //fprintf(dlog, "Step 2 IMAP Message Sent = %s\n", Buffer);  /* print message */
    //fflush(dlog);

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);        /* print message */
    //fflush(dlog);

    Helper = (char *) malloc(IMAP_MSG_SIZE);   

    strcpy(Buffer, "3 FETCH 1:* ");            /* load buffer */
    strcat(Buffer, "(body[header.fields (X-SecNetIE-TLP from subject date)])\n");

    write(Socket, Buffer, strlen(Buffer));          /* write data */
    //printf("4");

    Buffer[strlen(Buffer)] = '\0';
    //fprintf(dlog, "Step 3 IMAP Message Sent = %s\n", Buffer);  /* print message */
    //fflush(dlog);

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //printf("IMAP Message Received = %s\n", Buffer);  /* print message */

    strcpy(Helper, "4 LOGOUT\n");            /* use helper now, because "Buffer holds*/
                                             /*the info we need*/
    
    Helper[strlen(Helper)] = '\0';
    //fprintf(dlog, "Step 4 IMAP Message Sent = %s\n", Helper);  /* print message */
    //fflush(dlog);
    
    write(Socket, Helper, strlen(Helper));  /* write data */
      
    recv(Socket, Helper, 1024, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Helper); /* print message */
    //fflush(dlog);

  }
  else if ( !strcmp(This_Session[0], "body") )
  {
    strcpy(Buffer, "1 LOGIN ");                  /* load buffer */
    strcat(Buffer, This_Session[3]);            /* append username */
    strcat(Buffer, " ");
    strcat(Buffer, This_Session[4]);              /* append password */ 
    strcat(Buffer, "\n");
    write(Socket, Buffer, strlen(Buffer));          /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);    /* print message */

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);
    
    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer); /* print message */
    //fflush(dlog);

    //if login fails just return with latest response in the buffer
    if (strstr(Buffer, "1 NO Authentication failed."))
    {
	close(Socket);
        return Buffer;
    }
      
    strcpy(Buffer, "2 SELECT ");                       /* load buffer */
    strcat(Buffer, This_Session[3]);                   /* append username */
    //strcat(Buffer, "/");
    //strcat(Buffer, This_Session[3]);                  /* append password */ 
    strcat(Buffer, "\n");
    write(Socket, Buffer, strlen(Buffer));            /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);      /* print message */

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);   /* print message */
    //fflush(dlog);

    strcpy(Buffer, "3 FETCH ");                            /* load buffer */
    strcat(Buffer, This_Session[1]);
    strcat(Buffer, " body[header.fields (X-SecNetIE-Tag X-SecNetIE-TLP from subject date to message-id in-reply-to)]\n"); 
    write(Socket, Buffer, strlen(Buffer));                /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);        /* print message */
    
    //recv(Socket, Buffer, 65535, 0);
    //strcat(Buffer, "\n\n"); // just in case we want to keep the headers
    recv(Socket, msgHeaders, 2048, 0);

    Helper = (char *) malloc(1024); /*helper string for temp storage*/

    strcpy(Helper, "4 FETCH ");                            /* load buffer */
    strcat(Helper, This_Session[1]);
    strcat(Helper, " body[TEXT]\n"); 
    //strcat(Helper, " body[RFC822.TEXT]\n"); //not supported
    write(Socket, Helper, strlen(Helper));                /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);        /* print message */
    
    //recv(Socket, Buffer + strlen(Buffer), 65535 - strlen(Buffer), 0); //keep the headers
    //recv(Socket, Buffer, 65535, 0); //overwrite buffers
    
    recv(Socket, msgBody, IMAP_MSG_SIZE, 0); 
    
    //printf("IMAP Message Received = %s\n", Buffer);  /* print message */


    strcpy(Helper, "5 LOGOUT\n");                  /* load buffer */
    //printf("IMAP Message Sent = %s\n", Helper);  /* print message */
    write(Socket, Helper, strlen(Helper));         /* write data */

    //i = read(Socket, Helper, 1024);                  /* read message */
    recv(Socket, Helper, 1024, 0);
    //Helper[i] = '\0';

    //fprintf(dlog, "IMAP Message Received = %s\n", Helper);  /* print message */
    //fflush(dlog);

  }
  else if ( !strcmp(This_Session[0], "delete") )
  {
    strcpy(Buffer, "1 LOGIN ");              /* load buffer */
    strcat(Buffer, This_Session[3]);         /* append username */
    strcat(Buffer, " ");
    strcat(Buffer, This_Session[4]);         /* append password */ 
    strcat(Buffer, "\n");
    write(Socket, Buffer, strlen(Buffer));   /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);     /* print message */

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);  /* print message */
    //fflush(dlog);

    //if login fails just return with latest response in the buffer
    if (strstr(Buffer, "1 NO Authentication failed."))
    {
	   close(Socket);
       return Buffer;
    }
      
    strcpy(Buffer, "2 SELECT ");            /* load buffer */
    strcat(Buffer, This_Session[3]);        /* append username */
    //strcat(Buffer, "/");
    //strcat(Buffer, This_Session[3]);        /* append password */ 
    strcat(Buffer, "\n");
    write(Socket, Buffer, strlen(Buffer));  /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);  /* print message */

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);   /* print message */
    //fflush(dlog);

    strcpy(Buffer, "3 STORE ");                  /* load buffer */
    strcat(Buffer, This_Session[1]);              
    strcat(Buffer, " +FLAGS (\\DELETED)\n");   /*mark msg as deleted*/
    write(Socket, Buffer, strlen(Buffer));      /* write data */
    //printf("IMAP Message Sent = %s\n", Buffer);  /* print message */

    recv(Socket, Buffer, IMAP_MSG_SIZE, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Buffer);  /* print message */
    //fflush(dlog);

    Helper = (char *) malloc(1024);

    strcpy(Helper, "4 EXPUNGE\n");                 /* really delete those msgs */
    //printf("IMAP Message Sent = %s\n", Helper);  /* print message */
    write(Socket, Helper, strlen(Helper));         /* write data */

    recv(Socket, Helper, 1024, 0);
    //fprintf("IMAP Message Received = %s\n", Helper); /* print message */
    //fflush(dlog);

    strcpy(Helper, "4 LOGOUT\n");                   /* load buffer */
    //printf("IMAP Message Sent = %s\n", Helper);   /* print message */
    write(Socket, Helper, strlen(Helper));          /* write data */

    recv(Socket, Helper, 1024, 0);

    //fprintf(dlog, "IMAP Message Received = %s\n", Helper); /* print message */
    //fflush(dlog);

  }

  close(Socket);
  
  //free(Token); 
  //free(Helper); 
  //free(buf);

  return Buffer;  /*return the important data stored in buffer*/

}


char* getTLPvalue3(char* s)
{
        
    char* s2 = (char*) malloc(2048);
    strcpy(s2, s); 
    
    char* t = strtok(s2, " \n\x0A\x0D");
    
    while (t = strtok(NULL, " \n\x0A\x0D"))
    {
        if (!strcmp(t,"X-SecNetIE-TLP:"))
        {
            t = strtok(NULL, " \n\x0A\x0D");
            /*
            if (!strcmp(t,"amber")) 
                return "orange";
            else
            */
                return t;
        }
    }
    
    return "gray";
}


char* my_itoa(int n)
{
    char* buf = (char*) malloc(32);
    sprintf(buf, "%d", n);
    return buf;
}


char x2c(char *s) 
{
  char d;
  d = (s[0] >= 'A' ? ((s[0] & 0xdf) - 'A')+10 : (s[0] - '0'));
  d *= 16;
  d += (s[1] >= 'A' ? ((s[1] & 0xdf) - 'A')+10 : (s[1] - '0'));
  return(d);
}

int parse_var(const char* env)
{
  char *s, *ptr; 
  int i, j, k, n;  
  
  if ( (s = (char *) getenv(env)) == NULL ) 
    return(0);  

  //diag(); exit(1);	  
                  
  if ( !strcmp(env, "QUERY_STRING") ||
       !strcmp(env, "HTTP_COOKIE")    )
  {
    ptr = s;
    n = strlen(s);
    s = (char *)malloc(n+1);
    strcpy(s, ptr);              
  } 
  else if (!strcmp(env, "CONTENT_LENGTH"))
  {
    n = atoi(s);
    ptr = s = (char *)malloc(n+1);
    while ( (i = read(0, ptr, n - (ptr - s))) > 0 )
      ptr += i;
    s[n] = '\0';
  }
  
  if (strcmp(env, "HTTP_COOKIE"))
  {
    for (i = 0; i < n; i++) 
      if ( s[i] == '+' ) s[i] = ' ';
      else if ( s[i] == '=' || s[i] == '&' ) s[i] = '\0';
  }
  else
    for (i = 0; i < n; i++) 
      if ( s[i] == '+' ) s[i] = ' ';
      else if ( s[i] == '=' || s[i] == ';' ) s[i] = '\0';
 
  
  for (i = 0, j = 0; j < n; i++, j++)
    if ((s[i] = s[j]) == '%') 
    {
      s[i] = x2c(&s[j+1]);
      j += 2;
    }
  
  s[i] = '\0';
  
  for (name[0] = s, n = 1, j = 0, k = 0; j < i && k < STRING_SIZE; j++) 
  {
    if ( s[j] == '\0' ) 
    {
      if ( n == 0 )
        name[k] = s + j + 1;
      else value[k++] = s + j + 1;
      
      n = 1 - n;
    }
  }

  //diag(); exit(1);
  
  return(k);
            
}


char* getHeaderElements(char* s)
{
    char* s2 = (char*) malloc(2048);
    
    char **elements = (char**) malloc(32); elements[0] = NULL;
    char **elements2 = (char**) malloc(32); elements2[0] = NULL;
    
    //printf("in getHeaderEleements"); exit(1);

    strcpy(s2, s);
 
    char *t = strtok(s2, "\n\x0A\x0D");
    elements[0] = (char*) malloc(1024);
    strcpy(elements[0], t);
    elements[1] = NULL;
    
    //printf("\nelements[0]=%s", elements[0]); exit(1);

    int i = 1;

    while (t = strtok(NULL, "\n\x0A\x0D"))
    {   
        elements[i] = (char*) malloc(1024);
        elements[i+1] = NULL;
        strcpy(elements[i++], t);
        //printf("\nelements[%d]=%s", i-1, elements[i-1]);
    }
    
    //exit(1);
    
    int j;
    for (j = 0; j < i; j++)
    {
        elements2[j] = strstr(elements[j], ": ") + 2;
        elements2[j+1] = NULL;
        //printf("\nelements2[%d]=%s", j, elements2[j]);
    }
        
    return elements2;
}


char* cleanIMAPresponse(char *s)
{
    char* start = strchr(s, '}') + 1;
    char* end = strrchr(s, ')') - 2;

    *end = 0;
    
    return start;
}


void outputHeaders()
{
    int i;
    char **elements = NULL;
    char* tlpcolor;
        
    //printf("got in headers"); exit(1);
    printSnippet("InboxStart", This_Session[3], my_itoa(HeaderNum), NULL);
    //printf("got in headers 2"); exit(1);    
    
    if(HeaderNum)
	{       

          //printf("HeaderNum exists"); exit(1);

		for (i = 0; i < HeaderNum; i++)
        {
            elements = getHeaderElements(Headers[i]);
            
            /*
            int j = 0;
            while (elements[j]) printf("%s<br>", elements[j++]);
            printf("----<br>");
            exit(1);
            */
                        
            if (strcmp(elements[1], "red") && strcmp(elements[1], "amber") && strcmp(elements[1], "green") && strcmp(elements[1], "white"))
                tlpcolor = "gray";         
            else if (!strcmp(elements[1], "amber"))
                 tlpcolor = "orange";
            else
                tlpcolor = elements[1];
                                   
            if (!strcmp(tlpcolor, "gray"))
                printSnippet("InboxElement", tlpcolor, "error", elements[1], my_itoa(i+1), elements[2], elements[0], NULL);
            else
                printSnippet("InboxElement", tlpcolor, elements[1], elements[3], my_itoa(i+1), elements[0], elements[2], NULL); 
        }	
	}    
	
    
    printSnippet("InboxEnd", NULL);        
    
}


void outputBody()
{
    //printf("TLP_Value is: %s and its length is %d", TLP_Value, strlen(TLP_Value)); exit(1);
    
    //diag();
    
    if (!strcmp(TLP_Value, "red")){
        printSnippet("DisableCopying", NULL);
    }
    
    //printf("<h1>%s</h1>", TLP_Value);
    
    printSnippet("MessageDisplayStart", NULL);
    
    if (!strcmp(TLP_Value, "amber") || !strcmp(TLP_Value, "green") || !strcmp(TLP_Value, "white")){
        printSnippet("MessageOptionsFull", This_Session[1], NULL);
    }
    else
    {
        printSnippet("MessageOptionsDelete", This_Session[1], NULL);
    }
    
    char* tlpcolor = TLP_Value;
    
    if (!strcmp(TLP_Value, "amber")) tlpcolor = "orange"; 
    
    char* secnetTag = (char*) malloc(1024); strcpy(secnetTag, "");
    
    char* startp = strstr(msgHeaders, "X-SecNetIE-Tag: ");     
    char* stopp;
    
    if (startp){
        startp += 16;
        stopp = strstr(startp, "\n");
        memcpy(secnetTag, startp, stopp-startp);                
        *(secnetTag + (stopp-startp) - 1) = NULL;
    }
    
    printSnippet("MessageDisplayEnd", tlpcolor, cleanIMAPresponse(msgHeaders), cleanIMAPresponse(msgBody), secnetTag, NULL);
}


/****************************************************************************/
void diag()
{
    printf("---This_Session<br>");
    int i;
    for (i=0; i < 20; i++){
        printf("%d = %s",i, This_Session[i]); 
        printf("<br>");
    }
    
    printf("---<br>");
    
    printf("---value<br>");
    for (i=0; i < 20; i++){
        printf("%d = %s",i, value[i]);
        printf("<br>");
    }
    
    printf("---<br>");

}
/***************************************************************************/

char* generateHierarchyTree()
{
    xsltStylesheetPtr cur = NULL;
    xmlDocPtr doc, res;
    
    int size = 0;
    char* html = (char*) malloc(TREE_SIZE);
    
    char* xslFilePath = (char*) malloc(1024); *xslFilePath = 0;
    char* xmlFilePath = (char*) malloc(1024); *xmlFilePath = 0;

    strcat(xslFilePath, secnet_path); strcat(xslFilePath, "/sys/html/xml/insaw.xsl");
    strcat(xmlFilePath, secnet_path); strcat(xmlFilePath, "/sys/html/xml/insaw.xml");

    cur = xsltParseStylesheetFile((const xmlChar *) xslFilePath);        
    
    doc = xmlParseFile(xmlFilePath);    
    
    res = xsltApplyStylesheet(cur, doc, NULL);    
    
    
    xsltSaveResultToString((xmlChar **)&html, &size, res, cur); //*(html + size) = NULL;
    //printf("got to here4"); exit(1);
    
    
    xsltFreeStylesheet(cur);
    xmlFreeDoc(res);
    xmlFreeDoc(doc);
    xsltCleanupGlobals();
    xmlCleanupParser();    
    
    //printf("***********************\n\n%s", strstr(html, "<div")); exit(1);
    
    return strstr(html, "<div");
}

int login()
{
    S = IMAP_operation();

    //if login fails just return with latest response in the buffer
    if (strstr(S, "1 NO Authentication failed."))
        return 0;
    else
        return 1;
}

void myexit(int ExitCode, char *Msg)
{
    printf("%s", Msg);
    exit(ExitCode);
}

int sendMessage(char* hostname, char* mail_from, char* rcpt_to, char* tlp_color, char* subject, char* message, char* in_reply_to, char* tag)
{
    int  InSockHandle, OutSockHandle, i, Len;
    struct hostent *SMTPHost, *CGIHost;
    char InBuf[1024], OutBuf[65536], host[64], *ptr;
    struct sockaddr_in InSockAddr, OutSockAddr;

    /*Setup OutSocket*/
    if ( (OutSockHandle = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) /* make socket */
        myexit(1, "OutSock could not be created!\n");
    if ( (SMTPHost = gethostbyname(hostname)) == 0 )             /* get host */
        myexit(1, "SMTP Host could not be resolved!\n");

    /*Setup Socket*/
    memset(&OutSockAddr, 0, sizeof(struct sockaddr_in));
    memcpy(&OutSockAddr.sin_addr, SMTPHost->h_addr, SMTPHost->h_length);/* set host */
    OutSockAddr.sin_port = htons(SMTP_PORT);                        /* set port */
    OutSockAddr.sin_family = AF_INET;
  
    /*Setup InSocket to get mail message */
    //if ( (InSockHandle = socket(AF_INET, SOCK_STREAM, 0)) < 0 )/* create socket */
    //myexit(1, "InSock could not be created!");
  
    if ( connect( OutSockHandle, (struct sockaddr *)&OutSockAddr, sizeof(OutSockAddr) ) < 0 )
        myexit(1, "OutSock could not be connected to SMTP host!");

    i = read(OutSockHandle, InBuf, 1024);                     /* read message */
    InBuf[i] = '\0';
    //printf("Message Received = %s\n", InBuf);               /* print message */

    strcpy(OutBuf, "HELO "); strcat(OutBuf, hostname); strcat(OutBuf, "\n"); /* load buffer */
  
    write(OutSockHandle, OutBuf, strlen(OutBuf));           /* write data */
    //printf("Message Sent = %s\n", OutBuf);               /* print message */

    i = read(OutSockHandle, InBuf, 1024);                      /* read message */
    InBuf[i] = '\0';
    printf("Message Received = %s\n", InBuf);               /* print message */

    strcpy(OutBuf, "MAIL FROM:");  /* load buffer */
    strcat(OutBuf, mail_from); 
    strcat(OutBuf, "\n");
    write(OutSockHandle, OutBuf, strlen(OutBuf));         /* write data */
    printf("Message Sent = %s\n", OutBuf);                /* print message */

    i = read(OutSockHandle, InBuf, 1024);                 /* read message */
    InBuf[i] = '\0';
    printf("Message Received = %s\n", InBuf);             /* print message */
    //exit(0);

    char* tmp = (char*) malloc(2049); *tmp = 0;
    strncat(tmp,rcpt_to,2047);
    char *t = strtok(tmp,";");
  
    while(t)
    {
        strcpy(OutBuf, "RCPT TO:");                         /* load buffer */
        strcat(OutBuf, t); 
        strcat(OutBuf, "\n");
        write(OutSockHandle, OutBuf, strlen(OutBuf));        /* write data */
        printf("Message Sent = %s\n", OutBuf);               /* print message */
    
        i = read(OutSockHandle, InBuf, 1024);                /* read message */
        InBuf[i] = '\0';
        printf("Message Received = %s\n", InBuf);            /* print message */
    
        t = strtok(NULL, ";");
    }
  
    free(tmp);
  
    strcpy(OutBuf, "DATA\n");  /* load buffer */
    write(OutSockHandle, OutBuf, strlen(OutBuf));
    printf("Message Sent = %s\n", OutBuf);               /* print message */

    i = read(OutSockHandle, InBuf, 1024);                /* read message */
    InBuf[i] = '\0';
    printf("Message Received = %s\n", InBuf);            /* print message */

    strcpy(OutBuf, "Subject: ");                         /* load buffer */
    strcat(OutBuf, subject);
    strcat(OutBuf, "\n");

    strcat(OutBuf, "X-SecNetIE-TLP: ");                         /* TLP header*/
    strcat(OutBuf, tlp_color);
 
    strcat(OutBuf, "\n");
  
    if (in_reply_to && strlen(in_reply_to)){
        strcat(OutBuf, "In-Reply-To: ");                         /* replies only: In-Reply-To header*/
        strcat(OutBuf, in_reply_to);
 
        strcat(OutBuf, "\n");
    }
    
    if (tag && strlen(tag)){
        strcat(OutBuf, "X-SecNetIE-Tag: ");                         /* replies only: In-Reply-To header*/
        strcat(OutBuf, tag);
 
        strcat(OutBuf, "\n");
    }    
  
    strcat(OutBuf, "To: ");                         /* To: header*/
    strcat(OutBuf, rcpt_to);
 
    strcat(OutBuf, "\n\n");  

    strcat(OutBuf, message);
    strcat(OutBuf, " ");

    strcat(OutBuf, "\n.\n");
    write(OutSockHandle, OutBuf, strlen(OutBuf));        /* write data */
    printf("Message Sent = %s\n\n\n", OutBuf);               /* print message */

    i = read(OutSockHandle, InBuf, 1024);                /* read message */
    InBuf[i] = '\0';
    printf("Message Received = %s\n", InBuf);            /* print message */

    strcpy(OutBuf, "QUIT\n");                            /* load buffer */
    write(OutSockHandle, OutBuf, strlen(OutBuf));        /* write data */
    printf("Message Sent = %s\n", OutBuf);               /* print message */

    i = read(OutSockHandle, InBuf, 1024);                /* read message */
    InBuf[i] = '\0';
    printf("Message Received = %s\n", InBuf);            /* print message */

    /* close sockets */
    close(OutSockHandle);    
  //close(InSockHandle);
}

char* processReplyMessage(char* s)
{
    char* newMsg = (char*) malloc(IMAP_MSG_SIZE); *newMsg = NULL;
    
    strcpy(newMsg, "\n\n\n---- Replied to message follows -------------\n");
    
    char** msgTokens = split2(s, "\n");
    
    //return my_itoa(salen(msgTokens));
        
    int i = 0; char* tmps;  
              
    int len = salen(msgTokens);
    
    while (i < len)
    { 
        tmps = msgTokens[i++];
        strcat(newMsg, "> ");
        strcat(newMsg, tmps);
        strcat(newMsg, "\n");
        //printf(tmps);
    }
    
    //freeStringArray(msgTokens);
    //printf(newMsg);
    return newMsg;
}

main()
{
    int counter;
    
    for (counter = 0; counter < STRING_SIZE; counter++) value[counter] = NULL;
    
	int i, k, pid;
	char *s;
	
	msgHeaders = (char *) malloc(2048); *msgHeaders = 0;
	msgBody = (char *) malloc(IMAP_MSG_SIZE); *msgBody = 0;
	
	//if ( (secnet_path = (char *) getenv("SECNET_PATH")) == NULL ) 
	    //return(10);	
        secnet_path = "/secnet";
	
    	//open log file for appending. How is concurrency dealt with?
	//dlog = fopen("imapd.log", "a");  
	
	printf("Content-type: text/html\r\n\r\n"); 	      
    
    if ( !parse_var("HTTP_COOKIE") )
    {	 
        //printSnippet("GenericProblemNotification", "You are not logged in!", NULL);
        //exit(0);        
        printSnippet("Login", "You are not logged in!", NULL);
	//printf("<h2>after snippet</h2>\n"); exit(0);
        exit(0);
    }

    
    
    char* user = (char*) malloc(1024); char* pass = (char*) malloc(1024);
    
    char* host = "node1.enercom.de";
    
    strcpy(user, value[0]); strcpy(pass, value[1]);
        
    //clean up values
    //for (counter = 0; counter < STRING_SIZE; counter++) value[counter] = NULL;
    value[0] = NULL;
    
    //diag(); exit(1);
    
    if ( !parse_var("CONTENT_LENGTH") ) parse_var("QUERY_STRING");
    
    if (!value[0]) value[0] = "headers";
    
    //diag(); exit(1);
    
    
    This_Session[0] = NULL;

    This_Session[2] = host;

    //s = (char *) malloc(1024);
    //strcpy(s, value[0]);
    This_Session[3] = user;

    //s = (char *) malloc(1024);
    //strcpy(s, value[1]);
    This_Session[4] = pass;

    This_Session[5] = "";
    This_Session[6] = host;    
    
    
    // SOS - Later get the name value pairs and assign to dictionary (associative array)
    // then use only those, like: session("name") 

    

    /*Set appropriate msg body number*/  
    if (!strcmp(value[0], "headers"))
    {
        value[1] = "-1";
        This_Session[0] = value[0];
        This_Session[1] = value[1];
    }
    else if (!strcmp(value[0], "delete"))
    {
        This_Session[0] = value[0];
        This_Session[1] = value[1];        
    }
    else
    {
        This_Session[1] = value[0];
        This_Session[0] = value[1];    
    }
    
    //diag();exit(1);
  
    S = IMAP_operation();

    //if login fails just return with latest response in the buffer
    if (strstr(S, "1 NO Authentication failed."))
    {
        printSnippet("Login", "Wrong username or password", NULL);
        exit(0);
    }
    else if (strstr(S, "1 NO"))
    {
        printSnippet("GenericProblemNotification", "Problem connecting to IMAP server!", NULL);
        exit(0);
    }
    
    //diag();exit(1);

    printSnippet("PageStart", NULL);
    printSnippet("LeftPanelStart", NULL);
    //printSnippet("LeftPanelEnd", NULL);
    printSnippet("MiddlePanelStart", NULL);

    //diag();exit(1);
    
    if (value[0] && !strcmp(value[0], "headers"))
    {
        sToken = strtok(S, "\n");
        makeSubstrings2("\n");
        //diag();exit(1);
        outputHeaders();
    }
    else if (value[1] && !strcmp(value[1], "body"))
    {
        TLP_Value = getTLPvalue3(msgHeaders);
        outputBody();
    }
    else if (value[0] && !strcmp(value[0], "compose"))
    {
        //diag();        
        char* recipients = (char*) malloc(10240); *recipients = NULL;
        char* from = (char*) malloc(1024); *from = NULL;
        
        strcpy(from, user); strcat(from, "@"); strcat(from, host);
        
        int i = 1;
        while (value[i] && strstr(value[i], "@")){                                 
            strcat(recipients, value[i++]);
            strcat(recipients, "; ");
        }
        if (i > 1) *(recipients + strlen(recipients) - 1) = NULL;
        
        //printf("%s", from);
        printSnippet("MessageEdit", from, recipients, "", "", "", NULL);        
    }   
    else if(value[5] && !strcmp(value[5], "send"))
    {
        //diag(); exit(1);
        sendMessage(host, value[0], value[1], value[2], value[3], value[4], value[6], value[7]);
        printSnippet("SentConfirmation", NULL);
    }
    else if(value[0] && !strcmp(value[0], "reply"))
    {
        //diag();       
        char* msg = (char*) malloc(IMAP_MSG_SIZE); *msg = NULL;
        char* msgRecipients = (char*) malloc(10240); *msgRecipients = NULL;
        char* msgFrom = (char*) malloc(1024); *msgFrom = NULL;
        char* msgSubject = (char*) malloc(1024); *msgSubject = NULL;
        char* msgInReplyTo = (char*) malloc(1024); *msgInReplyTo = NULL;
        
        //parse headers out and apply them in compose message form fields
        strcpy(msg, value[2]);
        strcpy(msgFrom, user); strcat(msgFrom, "@"); strcat(msgFrom, host);
        
        char** msgTokens = split3(msg, "\n\r");
        
        int i = 0; char* tmps;                 
           
        int len = salen(msgTokens);
    
        while (i < len)
        {        
            tmps = msgTokens[i++];
            
            if ( strstr(tmps, "Subject: ") == tmps )
            {       
                strcpy(msgSubject, "RE: ");
                strcat(msgSubject, tmps + 9);
            }                        
            else if ( strstr(tmps, "From: ") == tmps )
            {
                strcpy(msgRecipients, tmps + 6);
                //strcat(msgRecipients, ";");
            }     
            else if ( strstr(tmps, "Message-Id: ") == tmps) 
            {
                strcpy(msgInReplyTo, tmps + 12);
            }
            //if ((tmps = strstr(msgTokens[i], "X-SecNetIE-TLP: ")) == msgTokens[i])        msgTLP        = tmps + 16;
        }
        
        //printf("%s", from);
        char* nmsg = processReplyMessage(msg);
        
        //printf(ns);
        if (strlen(msgInReplyTo))
            printSnippet("MessageEdit", msgFrom, msgRecipients, msgSubject, nmsg, msgInReplyTo, NULL);
        else
            printSnippet("MessageEdit", msgFrom, msgRecipients, msgSubject, nmsg, "", NULL);        
        
        /*
        freeStringArray(msgTokens);
        free(msg); 
        free(msgRecipients); 
        free(msgFrom);
        free(msgSubject);
        */
        
        //diag();
    }  
    else if(value[0] && !strcmp(value[0], "forward"))
    {
        //diag();       
        char* msg = (char*) malloc(IMAP_MSG_SIZE); *msg = NULL;
        //char* msgRecipients = (char*) malloc(10240); *msgRecipients = NULL;
        char* msgFrom = (char*) malloc(1024); *msgFrom = NULL;
        char* msgSubject = (char*) malloc(1024); *msgSubject = NULL;
        
        //parse headers out and apply them in compose message form fields
        strcpy(msg, value[2]);
        strcpy(msgFrom, user); strcat(msgFrom, "@"); strcat(msgFrom, host);
        
        char** msgTokens = split3(msg, "\n\r");
        
        int i = 0; char* tmps;                 
           
        int len = salen(msgTokens);
    
        while (i < len)
        {        
            tmps = msgTokens[i++];
            
            if ( strstr(tmps, "Subject: ") == tmps )
            {       
                strcpy(msgSubject, "FW: ");
                strcat(msgSubject, tmps + 9);
            }     
        }
        
        //printf("%s", from);
        char* nmsg = processReplyMessage(msg);
        

        printSnippet("MessageEdit", msgFrom, "", msgSubject, nmsg, "", NULL);        

    }      
    else if (value[0] && !strcmp(value[0], "delete"))
    {        
        //diag();exit(1);
        printSnippet("DeletionConfirmation", NULL);
    } 
    else
    {
        printf("<h2>Not implemented</h2>");
        diag();
    }        

    printSnippet("MiddlePanelEnd", NULL);
    
    char *tree = generateHierarchyTree();

    //printf("after first tree call"); exit(1); 
   
    printSnippet("RightPanelStart", tree, NULL);
    //free(tree);
    //printSnippet("RightPanelEnd", NULL);
    printSnippet("PageEnd", NULL); 
    
    
    //free(msgBody); free(msgHeaders);
    //fclose(dlog);
}
