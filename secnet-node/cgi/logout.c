#include <stdio.h>

main()
{ 
    /*Ouput Cookie header*/
    printf("Set-cookie: username=; path=/; domain=node1.enercom.de; "
            "expires=Thursday, 22-Mar-00 00:00:00 GMT\r\n");
    printf("Set-cookie: password=; path=/; domain=node1.enercom.de; "
            "expires=Thursday, 22-Mar-00 00:00:00 GMT\r\n");

    /*Output Document Type Header*/
    //printf("Content-type: text/html\n\n");
    //printf("Logged out! Back to <a href=\"/cgi-bin/recvcgi\">SecNet-IE</a>");
    printf("Status: 302 Moved\r\nLocation: /cgi-bin/recvcgi\r\n\r\n");
}
