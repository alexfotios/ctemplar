# compile the cgi *.c files, that are now under $SECNET_PATH/src/cgi
# dump the binaries directly into apache's cgi-bin dir
export SECNET_PATH="/secnet"

export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib"

cd $SECNET_PATH/src/cgi
#PATHS=`ls *.c`

#for i in $PATHS
#do
#	 gcc -I/usr/include/libxml2 -L/usr/lib -lxml2 -lxslt $i -o $SECNET_PATH/sys/apache/cgi-bin/`echo $i | grep -o '[a-z]*'` $i
#done

rm -rf $SECNET_PATH/sys/apache/cgi-bin/recvcgi
rm -rf $SECNET_PATH/sys/apache/cgi-bin/login
rm -rf $SECNET_PATH/sys/apache/cgi-bin/logout

gcc -I/usr/include/libxml2 -L/usr/lib -lxml2 -lxslt recvcgi.c -o recvcgi
gcc -I/usr/include/libxml2 -L/usr/lib -lxml2 -lxslt login.c -o login
gcc -I/usr/include/libxml2 -L/usr/lib -lxml2 -lxslt logout.c -o logout

cp ./recvcgi $SECNET_PATH/sys/apache/cgi-bin
cp ./login $SECNET_PATH/sys/apache/cgi-bin
cp ./logout $SECNET_PATH/sys/apache/cgi-bin

# set their permissions to 0755
#chmod 0755 $SECNET_PATH/sys/apache/cgi-bin/*
