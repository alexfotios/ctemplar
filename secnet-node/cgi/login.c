#include <stdio.h>
#include <string.h>

#define STRING_SIZE 1024

char *name[STRING_SIZE], *value[STRING_SIZE];

char x2c(char *s) 
{
    char d;
    d = (s[0] >= 'A' ? ((s[0] & 0xdf) - 'A')+10 : (s[0] - '0'));
    d *= 16;
    d += (s[1] >= 'A' ? ((s[1] & 0xdf) - 'A')+10 : (s[1] - '0'));
    return(d);
}

int parse_var(const char* env)
{
    char *s, *ptr; 
    int i, j, k, n;
  
    if ( (s = (char *) getenv(env)) == NULL ) 
        return(0);
    
                  
    if ( !strcmp(env, "QUERY_STRING") ||
          !strcmp(env, "HTTP_COOKIE")    )
    {
        ptr = s;
        n = strlen(s);
        s = (char *)malloc(n+1);
        strcpy(s, ptr);              
    } 
    else if (!strcmp(env, "CONTENT_LENGTH"))
    {
        n = atoi(s);
        ptr = s = (char *)malloc(n+1);
        while ( (i = read(0, ptr, n - (ptr - s))) > 0 )
            ptr += i;
        s[n] = '\0';
    }
  
    if (strcmp(env, "HTTP_COOKIE"))
    {
        for (i = 0; i < n; i++) 
            if ( s[i] == '+' ) s[i] = ' ';
        else if ( s[i] == '=' || s[i] == '&' ) s[i] = '\0';
    }
    else
        for (i = 0; i < n; i++) 
            if ( s[i] == '+' ) s[i] = ' ';
    else if ( s[i] == '=' || s[i] == ';' ) s[i] = '\0';
 
  
    for (i = 0, j = 0; j < n; i++, j++)
        if ((s[i] = s[j]) == '%') 
    {
        s[i] = x2c(&s[j+1]);
        j += 2;
    }
  
    s[i] = '\0';
  
    for (name[0] = s, n = 1, j = 0, k = 0; j < i && k < STRING_SIZE; j++) 
    {
        if ( s[j] == '\0' ) 
        {
            if ( n == 0 )
                name[k] = s + j + 1;
            else value[k++] = s + j + 1;
      
            n = 1 - n;
        }
    }

  //diag(); exit(1);
  
    return(k);
            
}

void diag()
{
    /*
    printf("---This_Session<br>");
    int i;
    for (i=0; i < 7; i++){
        printf("%d = %s",i, This_Session[i]);
        printf("<br>");
    }
    
    printf("---<br>");
    */
    
    int i;
            
    printf("---value<br>");
    for (i=0; i < 7; i++){
        printf("%d = %s",i, value[i]);
        printf("<br>");
    }
    
    printf("---<br>");

}

main()
{       
    //diag(); exit(1);
    
    if(parse_var("CONTENT_LENGTH") && !strcmp(value[2], "login"))
    {
        if (strlen(value[0]) && strlen(value[1]))
        {                        
            //printf("-%s-%s-\n\n", value[0], value[1]); exit(1);
            printf("Set-cookie: username=%s; path=/; domain=node1.enercom.de; "
                    "expires=Sunday 22-Mar-10 00:00:09 GMT\r\n", value[0]);
            printf("Set-cookie: password=%s; path=/; domain=node1.enercom.de; "
                    "expires=Sunday 22-Mar-10 00:00:09 GMT\r\n", value[1]);
            
            printf("Status: 302 Moved\r\nLocation: /cgi-bin/recvcgi\r\n\r\n");           
        }
        else
        {
            printf("Status: 302 Moved\r\nLocation: /cgi-bin/logout\r\n\r\n");
        }
    }
    else
    {
        printf("Status: 302 Moved\r\nLocation: /cgi-bin/logout\r\n\r\n");
    }    
}
