## setup hostname stuff
sync
echo Enter secnet-ie node hostname:
read HOSTNAME

sync
echo Enter secnet-ie node IP: 
read IP

sync
echo Enter secnet-ie node NETMASK: 
read NETMASK

sync
echo Enter secnet-ie node Gateway:
read GATEWAY

sync

hostname $HOSTNAME

echo 127.0.0.1  localhost.localdomain localhost > /etc/hosts
echo $IP $HOSTNAME >> /etc/hosts

echo NETWORKING=yes > /etc/sysconfig/network
echo HOSTNAME=$HOSTNAME >> /etc/sysconfig/network
echo GATEWAY=$GATEWAY >> /etc/sysconfig/network 
