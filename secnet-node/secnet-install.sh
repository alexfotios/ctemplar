function error {
	sync
	echo "ERROR: Press <enter> to continue..."
	read SOMETHING
}

trap error ERR

before="$(date +%s)"

## vars
export SECNET_PATH="/secnet"
export SOURCES_PATH="../src"
export CGI_SOURCES_PATH="./cgi"
export MAIL_FILTER_SOURCES_PATH="./mail-filter"
export HTML_SOURCES_PATH="./html"
export SECNET_CONF="./conf"
export SECNET_CERTS="./certs"

#script needs to be called from the dir where it resides!
# How can we fix this?
export SECNET_INSTALL_DIR=`pwd`

#first shut down and clean up current installation
$SECNET_INSTALL_DIR/secnet-stop.sh
rm -rf $SECNET_PATH

## Amend certain existing system vars
export PATH="$PATH:/usr/sbin" #make sure we have direct access to commands like useradd and groupadd
export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:/var/lib:/lib:/usr/lib:/usr/local/lib:$LD_LIBRARY_PATH"

# call other shell scripts
echo ############################################################################
echo ############################################################################
echo secnet-install-init start
echo ############################################################################
./secnet-install-init.sh
cd $SECNET_INSTALL_DIR


echo ############################################################################
echo ############################################################################
echo secnet-install-openssl start
echo ############################################################################
./secnet-install-openssl.sh 
cd $SECNET_INSTALL_DIR


echo ############################################################################
echo ############################################################################
echo secnet-setup-CA and secneet-generate-certs start
echo ############################################################################
./secnet-setup-CA.sh
cd $SECNET_INSTALL_DIR
./secnet-generate-certs.sh
cd $SECNET_INSTALL_DIR


echo ############################################################################
echo ############################################################################
echo secnet-install-postfix and mail filter start
echo ############################################################################
./secnet-install-postfix.sh
cd $SECNET_INSTALL_DIR
./mail-filter/secnet-install-filter.sh
cd $SECNET_INSTALL_DIR


echo ############################################################################
echo ############################################################################
echo secnet-install-dovecot start
echo ############################################################################
./secnet-install-dovecot.sh
cd $SECNET_INSTALL_DIR


echo ############################################################################
echo ############################################################################
echo secnet-install-apache, cgi and html start
echo ############################################################################
./secnet-install-apache.sh
cd $SECNET_INSTALL_DIR
./cgi/secnet-install-cgis.sh
cd $SECNET_INSTALL_DIR
./html/secnet-install-html.sh
cd $SECNET_INSTALL_DIR
echo ############################################################################
echo ############################################################################
echo END END END

after="$(date +%s)"
elapsed_seconds="$(expr $after - $before)"
elapsed_minutes="$(expr $elapsed_seconds / 60)"
echo Elapsed seconds for code block: $elapsed_seconds
echo Elapsed minutes for code block: $elapsed_minutes
