## vars
export SECNET_PATH="/secnet"
#export SOURCES_PATH="/work/secnetie/secnet-src"
#export SECNET_INSTALL="/work/secnetie/secnet-dev" #to later point to wherever pre-intallation conf files will reside in the deployment CD
                                           #rename secnet-dev dir to secnet-conf
#export SECNET_CERTS="/secnet/sys/certs"

## Amend certain existing system vars - these should be added in system's startup files as well
export PATH="$PATH:/usr/sbin" #make sure we have direct access to commands like useradd and groupadd
export LD_LIBRARY_PATH="/secnet/sys/openssl/lib:$LD_LIBRARY_PATH"

$SECNET_PATH/sys/postfix/command/postfix start
$SECNET_PATH/sys/dovecot/sbin/dovecot
$SECNET_PATH/sys/apache/bin/httpd -f$SECNET_PATH/sys/apache/conf/httpd.conf -k start
